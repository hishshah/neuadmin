import { combineReducers } from "redux";
import { GET, GET_DETAIL, RESET_DETAIL } from "../actions/users";
import _ from "lodash";
import { defaultMeta } from '../global';

const is_fetching_list = (state = false, action) => {
  switch (action.type) {
    case GET:
      return true;
    default:
      return false;
  }
};

const list = (state = [], action) => {
  switch (action.type) {
    case GET + '_SUCCESS':
      if (_.get(action, 'payload.data.status', '') === 'success') {
        return _.sortBy(action.payload.data.data.user, ['id']);
      }
      return [];
    case GET + '_FAIL':
      return [];
    default:
      return state;
  }
}

const detail = (state = {}, action) => {
  switch (action.type) {
    case GET_DETAIL + '_SUCCESS':
      if (_.get(action, 'payload.data.status', '') === 'success') {
        return action.payload.data.data.user;
      }
      return {};
    case GET_DETAIL + '_FAIL':
    case RESET_DETAIL:
      return {};
    default:
      return state;
  }
}

const meta = (state = defaultMeta, action) => {
  switch (action.type) {
    case GET + '_SUCCESS':
      if (action.payload.data.status === 'success') {
        let { page, count, limit, totalPage } = action.payload.data.data
        return {
          page, count, limit, totalPage
        };
      }
      return defaultMeta;
    case GET + '_FAIL':
      return defaultMeta;
    default:
      return state;
  }
}

export default combineReducers({ list, is_fetching_list, detail, meta });