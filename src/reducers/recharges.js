import { combineReducers } from "redux";
import { GET, UPDATE } from "../actions/recharges";
import _ from "lodash";

const is_fetching_list = (state = false, action) => {
  switch (action.type) {
    case GET:
      return true;
    default:
      return false;
  }
};

const list = (state = [], action) => {
  switch (action.type) {
    case GET + '_SUCCESS':
      if (action.payload.data.status === 'success') {
        return _.sortBy(action.payload.data.data.product, ['id']);
      }
      return [];
    case GET + '_FAIL':
      return [];
    case UPDATE:
      const items = [...state];
      const key = items.map(x => x.id).indexOf(action.id);
      const selected = items[key];
      if (selected) {
        items[key].isActive = action.data.isActive;
      }
      return items;
    default:
      return state;
  }
}

const meta = (state = null, action) => {
  switch (action.type) {
    case GET + '_SUCCESS':
      if (action.payload.data.status === 'success') {
        let { page, count, limit, totalPage } = action.payload.data.data
        return {
          page, count, limit, totalPage
        };
      }
      return null;
    case GET + '_FAIL':
      return null;
    default:
      return state;
  }
}

export default combineReducers({ list, is_fetching_list, meta });