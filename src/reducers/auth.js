import { combineReducers } from "redux";
import { LOGIN, LOGOUT } from "../actions/auth";
import _ from "lodash";

const initState = {
  isAuthenticate: false,
  user: null
}

const auth = (state = initState, action) => {
  switch (action.type) {
    case LOGIN + '_SUCCESS':
      return {
        isAuthenticate: true,
        user: _.get(action, 'payload.data.data.user', null),
      }
    case LOGIN + '_FAIL':
      return initState;
    case LOGOUT:
      return initState;
    default:
      return state;
  }
}

const token = (state = '', action) => {
  switch (action.type) {
    case LOGIN + '_SUCCESS':
      return _.get(action, 'payload.data.data.token', '');
    case LOGIN + '_FAIL':
      return '';
    case LOGOUT:
      return '';
    default:
      return state;
  }
}

export default combineReducers({ auth, token });