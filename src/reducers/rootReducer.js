import { combineReducers } from 'redux'
import admin from './admin'
import auth from './auth'
import order from './order'
import payment from './payment'
import recharges from './recharges'
import sidebar from './sidebar'
import users from './users'

const rootReducer = combineReducers({
  admin,
  auth,
  order,
  payment,
  recharges,
  sidebar,
  users
})

export default rootReducer