import { combineReducers } from "redux";
import { UPDATE, GET } from "../actions/admin";
import _ from "lodash";
import { defaultMeta } from '../global';

const is_fetching_list = (state = false, action) => {
  switch (action.type) {
    case GET:
      return true;
    default:
      return false;
  }
};

const list = (state = [], action) => {
  switch (action.type) {
    case GET + '_SUCCESS':
      if (_.get(action, 'payload.data.status', '') === 'success') {
        _.each(action.payload.data.data.admin, admin => admin.checked = false);
        return _.sortBy(action.payload.data.data.admin, ['id']);
      }
      return [];
    case GET + '_FAIL':
      return [];
    case UPDATE:
      var items = [...state];
      const key = items.map(x => x.id).indexOf(action.item.id);
      const selected = items[key];
      if (selected) {
        items = {...action.item};
      }
      return items;
    default:
      return state;
  }
}

const meta = (state = defaultMeta, action) => {
  switch (action.type) {
    case GET + '_SUCCESS':
      if (action.payload.data.status === 'success') {
        let { page, count, limit, totalPage } = action.payload.data.data
        return {
          page, count, limit, totalPage
        };
      }
      return defaultMeta;
    case GET + '_FAIL':
      return defaultMeta;
    default:
      return state;
  }
}

export default combineReducers({ list, is_fetching_list, meta });