import React from 'react';
import logo from '../../assets/logo/maqmur_logo.svg';
import './Loading.scss';
import { Dimmer } from 'semantic-ui-react';

const Loading = () => {
  return (
    <Dimmer active={true} page>
      <img src={logo} alt="logo" className="loading" />
      <div>Mohon Tunggu</div>
    </Dimmer>
  );
};

export default Loading;