import React, { useState, useEffect } from 'react';
import { Breadcrumb, Segment, Grid, Table, Button, Label, Icon, Modal, TextArea, Form, Checkbox } from 'semantic-ui-react';
import { connect } from 'react-redux';
import Page from '../Page';
import { Link } from 'react-router-dom';
import moment from 'moment';
import NumberFormat from 'react-number-format';
import { getDetail } from '../../actions/order';
import _ from 'lodash';

const OrderDetail = (props) => {
  const [showCancelModal, setShowCancelModal] = useState(false);
  const [agreementCheck, setAgreementCheck] = useState(false);
  const [id, setID] = useState(props.match.params.id);
  const { order } = props;

  useEffect(() => {
    setID(props.match.params.id);
    props.getDetail(id);
    // eslint-disable-next-line
  }, [props.match.params.id]);

  return (
    <Page>
      <div className="container-content container-order">
        <Breadcrumb>
          <Breadcrumb.Section>Order</Breadcrumb.Section>
          <Breadcrumb.Divider icon='right angle' />
          <Breadcrumb.Section link><Link to="/order">Order List</Link></Breadcrumb.Section>
          <Breadcrumb.Divider icon='right angle' />
          <Breadcrumb.Section active>Detail Order</Breadcrumb.Section>
        </Breadcrumb>

        <h2 style={{marginTop: '24px'}}>Detail Order</h2>

        {_.get(order, 'User', false) &&
          <Segment style={{padding: '15px 25px'}}>
            <h3>{_.get(order, 'User.name', '-')}</h3>
            <Grid>
              <Grid.Row>
                <Grid.Column width={4}>User ID</Grid.Column>
                <Grid.Column width={12}>: {_.get(order, 'User.id', '-')}</Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column width={4}>Full Name</Grid.Column>
                <Grid.Column width={12}>: {_.get(order, 'User.name', '-')}</Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column width={4}>Phone Number</Grid.Column>
                <Grid.Column width={12}>: {_.get(order, 'User.phone', '-')}</Grid.Column>
              </Grid.Row>
            </Grid>
          </Segment>
        }

        <Segment style={{marginTop: '15px'}}>
          <div style={{display: 'flex', justifyContent: 'space-between'}}>
            <h3>Order ID: <span style={{fontWeight: 'normal'}}>{_.get(order, 'id', '-')}</span></h3>
            <div 
              style={{color: '#46A997', float: 'right', cursor: 'pointer'}} 
              onClick={() => props.getDetail(id)}
            >
              <Icon name="refresh" /> Refresh Status
            </div>
          </div>
          <Table basic="very">
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Date</Table.HeaderCell>
                <Table.HeaderCell>Order</Table.HeaderCell>
                <Table.HeaderCell>Amount</Table.HeaderCell>
                <Table.HeaderCell>Order Status</Table.HeaderCell>
                <Table.HeaderCell>Notes</Table.HeaderCell>
                <Table.HeaderCell>Action</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              <Table.Row verticalAlign="top">
                <Table.Cell>
                  <div style={{marginBottom: '13px'}}>
                    <strong>Payment updated at: </strong>
                    <div>{_.get(order, 'updatedAt', false) ? moment(order.updatedAt).format('D MMM YYYY, HH:mm') : '-'}</div>
                  </div>
                  <div style={{marginBottom: '13px'}}>
                    <strong>Expired at: </strong>
                    <div>{_.get(order, 'Payment.expiredAt', false) ? moment(order.Payment.expiredAt).format('D MMM YYYY, HH:mm') : '-'}</div>
                  </div>
                  <div style={{marginBottom: '13px'}}>
                    <strong>Created at: </strong>
                    <div>{_.get(order, 'createdAt', false) ? moment(order.createdAt).format('D MMM YYYY, HH:mm') : '-'}</div>
                  </div>
                </Table.Cell>
                
                <Table.Cell>
                  <div style={{marginBottom: '8px'}}>
                    <strong>Number: </strong>
                    {_.get(order, 'billNo', '-')}
                  </div>
                  <div style={{marginBottom: '8px'}}>
                    <strong>Amount: </strong>
                    <NumberFormat displayType="text" thousandSeparator={true} value={_.get(order, 'amount', '-')} />
                  </div>
                  <div style={{marginBottom: '8px'}}>
                    <strong>Description: </strong>
                    {_.get(order, 'note', '-') || '-'}
                  </div>
                </Table.Cell>

                <Table.Cell style={{color: '#46A997'}}>
                  <NumberFormat displayType="text" thousandSeparator={true} value={_.get(order, 'amount', '-')} prefix="Rp " />
                </Table.Cell>

                <Table.Cell>
                  <Label circular color={order.status === 'canceled' ? 'red' : order.status === 'completed' ? 'teal' : 'navy'}>{_.get(order, 'status', '-')}</Label>
                </Table.Cell>

                <Table.Cell>
                  {_.get(order, 'note', '-') || '-'}
                </Table.Cell>

                <Table.Cell>
                  {order.status === 'menunggu' ?
                    <Button basic fluid compact color="red" onClick={() => setShowCancelModal(true)}>
                      Cancel Order
                    </Button>
                  :
                    '-'
                  }
                </Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </Segment>
      </div>

      <Modal open={showCancelModal} onClose={() => setShowCancelModal(false)} size="small" className="container-order">
        <Modal.Header>
          CANCEL USER ORDER
          <span style={{float: 'right', cursor: 'pointer', fontSize: '25px'}} onClick={() => setShowCancelModal()}>&times;</span>
        </Modal.Header>
        <Modal.Content>
          <Form>
            <div style={{fontWeight: 'bold', marginBottom:'8px'}}>Reasons to cancel the user order</div>
            <TextArea 
              placeholder='Please write a reason to cancel user orders here ...' 
              style={{ minHeight: '100px', resize: 'none' }}  
              // value={refundReason}
              // onChange={(e, {value}) => setRefundReason(value)}
              maxLength={250}
            />
          </Form>
          <div style={{margin: '40px 0'}}>
            <Checkbox 
              label="I agree to be responsible for canceling this user's order" 
              checked={agreementCheck} 
              onChange={(e, {checked}) => setAgreementCheck(checked)}
            />
          </div>
          <div style={{textAlign: 'center'}}>
            <Button basic color="red" disabled={!agreementCheck}>
              YES, CANCEL ORDER
            </Button>
          </div>
        </Modal.Content>
      </Modal>
    </Page>
  );
};

const mapStateToProps = (state) => {
  return {
    order: state.order.detail,
  }
}

const mapDispatchToProps = (dispatch) => {
	return {
    getDetail: (id) => dispatch(getDetail(id)),
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(OrderDetail);