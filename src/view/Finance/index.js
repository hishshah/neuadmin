import React, { useState } from 'react';
import Page from '../Page';
import { Breadcrumb, Grid, Input, Segment, Table, Icon, Pagination, Button } from 'semantic-ui-react';
import './Finance.scss';
import moment from 'moment';
import NumberFormat from 'react-number-format';
import DatePicker from 'react-datepicker';

const Finance = () => {
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');

  const data = [
    {
      id: '1',
      date: '2020-07-15T07:30:00.000Z',
      total_balance: 100000
    },
    {
      id: '2',
      date: '2020-07-15T07:30:00.000Z',
      total_balance: 100000
    },
    {
      id: '3',
      date: '2020-07-15T07:30:00.000Z',
      total_balance: 100000
    },
    {
      id: '4',
      date: '2020-07-15T07:30:00.000Z',
      total_balance: 100000
    },
  ];

  const filterDate = () => {
    if (moment(startDate).isSameOrAfter(endDate)) {
      return;
    };
  };

  const resetFilter = () => {
    setStartDate('');
    setEndDate('');
  };

  const DatePickerInput = ({ value, onClick }) => (
    <Input 
      readonly 
      value={value} 
      onClick={onClick} 
      icon='calendar alternate outline'
      iconPosition='left'
      className="payment-datepicker"
      style={{marginRight: '30px'}}
      placeholder="DD/MM/YYYY"
    />
  );

  return (
    <Page>
      <div className="container-content container-finance">
        <Breadcrumb>
          <Breadcrumb.Section>Finance</Breadcrumb.Section>
          <Breadcrumb.Divider icon='right angle' />
          <Breadcrumb.Section active>Finance List</Breadcrumb.Section>
        </Breadcrumb>

        <Grid>
          <Grid.Row columns={2} style={{marginTop: '24px'}}>
            <Grid.Column>
              <h2>Finance List</h2>
            </Grid.Column>
          </Grid.Row>
        
          <Grid.Row as={Segment}>
            <Grid.Column style={{display: 'flex', alignItems: 'center'}}>
              Start
              <DatePicker 
                selected={startDate} 
                onChange={date => setStartDate(date)} 
                customInput={<DatePickerInput />}
                dateFormat="dd/MM/yyyy"
              />
              End
              <DatePicker 
                selected={endDate} 
                onChange={date => setEndDate(date)} 
                customInput={<DatePickerInput />}
                dateFormat="dd/MM/yyyy"
              />
              <Button color="black" style={{backgroundColor: '#405166'}} onClick={filterDate}>FILTER</Button>
              <span style={{color: '#46A997', fontWeight: 'bold', marginLeft: '24px', cursor: 'pointer'}} onClick={resetFilter}>Reset Filter</span>
              {startDate && endDate && moment(startDate).isSameOrAfter(endDate) &&
                <span style={{color: '#EC1E24', fontSize: '12px', marginLeft: '30px'}}>Start date cannot be later than end date.</span>
              }
            </Grid.Column>
          </Grid.Row>

          <Grid.Row as={Segment} style={{marginBottom: '30px', padding: '15px'}}>
            <Grid.Column>
              <Grid.Row style={{overflowX: 'scroll'}}>
                <Grid.Column>
                  <Table basic="very" unstackable fixed>
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell>Tanggal</Table.HeaderCell>
                        <Table.HeaderCell>Pukul</Table.HeaderCell>
                        <Table.HeaderCell>Total Neu User Balance in Sobatku</Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                    <Table.Body>
                      {data.map((item, i) => (
                        <Table.Row key={i} verticalAlign='top'>
                          <Table.Cell>{moment(item.date).format('DD MMMM YYYY')}</Table.Cell>
                          <Table.Cell>{moment(item.date).format('HH:mm')}</Table.Cell>
                          <Table.Cell><NumberFormat value={item.total_balance} displayType="text" thousandSeparator={true} /></Table.Cell>
                        </Table.Row>
                      ))}
                    </Table.Body>
                  </Table>
                </Grid.Column>
              </Grid.Row>
            </Grid.Column>
          </Grid.Row>

          {data.length > 0 &&
            <Grid.Row textAlign="center">
              <Grid.Column>
                <Pagination
                  defaultActivePage={1}
                  boundaryRange={0}
                  totalPages={10}
                  firstItem={{ content: <Icon name='angle double left' />, icon: true }}
                  lastItem={{ content: <Icon name='angle double right' />, icon: true }}
                  prevItem={{ content: <Icon name='angle left' />, icon: true }}
                  nextItem={{ content: <Icon name='angle right' />, icon: true }}
                />
                <span style={{margin: '0 24px'}}>1 - 10</span>
                <span style={{fontWeight: 'bold'}}>From 30 Entries</span>
              </Grid.Column>
            </Grid.Row>
          }
        </Grid>
      </div>
    </Page>
  );
};

export default Finance;