import React, { useState, useRef, useEffect, useCallback } from 'react';
import { connect } from 'react-redux';
import Page from '../Page';
import { Breadcrumb, Grid, Input, Segment, Table, Dropdown, Checkbox, Icon, Pagination, Button, Modal, Form } from 'semantic-ui-react';
import './Recharges.scss';
import _ from 'lodash';
import NumberFormat from 'react-number-format';
import Swal from 'sweetalert2';
import { getList, update, sync } from '../../actions/recharges';

const Recharges = (props) => {
  const [search, setSearch] = useState('');
  const [hoveredFilter, setHoveredFilter] = useState('');
  const [openFilter, setOpenFilter] = useState(false);
  const filterDropdown = useRef(null);
  const [filter, setFilter] = useState([
    {
      key: 'status',
      title: 'Status',
      children: [
        { value: 'true', text: 'Active', checked: false },
        { value: 'false', text: 'Inactive', checked: false },
      ]
    },
    {
      key: 'prepaid',
      title: 'Prepaid',
      children: [
        { value: 'telkomsel_data', text: 'Telkomsel', checked: false },
        { value: 'xl_data', text: 'XL', checked: false },
        { value: 'indosat_data', text: 'Indosat', checked: false },
        { value: 'three_data', text: 'Tri', checked: false },
        { value: 'smart', text: 'Smarfren', checked: false },
        { value: 'axis', text: 'Axis', checked: false },
        { value: 'pln_data', text: 'Token PLN', checked: false },
      ]
    },
    {
      key: 'postpaid',
      title: 'Postpaid',
      children: [
        { value: 'telkomsel_postpaid', text: 'Telkomsel', checked: false },
        { value: 'xl_postpaid', text: 'XL', checked: false },
        { value: 'indosat_postpaid', text: 'Indosat', checked: false },
        { value: 'three_postpaid', text: 'Tri', checked: false },
        { value: 'smartfren_postpaid', text: 'Smarfren', checked: false },
        { value: 'axis_postpaid', text: 'Axis', checked: false },
        { value: 'pln_postpaid', text: 'Tagihan PLN', checked: false },
      ]
    },
  ]);
  const [checkedFilter, setCheckedFilter] = useState([]);
  const [appliedFilter, setAppliedFilter] = useState([]);
  const [editData, setEditData] = useState({});
  const [showEditModal, setShowEditModal] = useState(false);
  const [nameError, setNameError] = useState(false);
  const [codeError, setCodeError] = useState(false);
  const [isSyncing, setIsSyncing] = useState(false);
  const { data, isFetching, meta } = props;

  useEffect(() => {
    props.getList();
    // eslint-disable-next-line
  }, [])

  const handleClickOutside = useCallback(e => {
    let dropdown = _.get(filterDropdown, 'current.ref.current.innerHTML', '');
    let target = _.get(e, 'target.innerHTML', '');
    if (!_.includes(dropdown, target)) {
      setOpenFilter(false);
      setHoveredFilter('');
    };
  }, []);

  useEffect(() => {
    window.addEventListener('mousedown', handleClickOutside);

    return () => {
      window.removeEventListener('mousedown', handleClickOutside);
    };
  }, [handleClickOutside]);

  const toggleFilter = (parentIndex, childIndex, value, type) => {
    let new_filter = [...filter];
    let item = new_filter[parentIndex].children[childIndex];
    item.checked = value;
    
    if (value) {
      setCheckedFilter([...checkedFilter, {...item, parent: parentIndex, index: childIndex}]);
    } else {
      let filter = _.filter(checkedFilter, checked => checked.value !== item.value)
      setCheckedFilter(filter);
      if (type === 'remove') applyFilter(1, filter);
    };

    setFilter(new_filter);
  };

  const resetFilter = () => {
    let new_filter = [...filter];
    _.each(new_filter, item => {
      _.each(item.children, child => {
        child.checked = false;
      });
    });
    setFilter(new_filter);
    setCheckedFilter([]);
    setAppliedFilter([]);
    let query = search.length > 0 ? `name%3D${search}` : '';
    props.getList(1, query);
  };

  const applyFilter = (page = 1, filter) => {
    setOpenFilter(false);
    if (!filter) filter = checkedFilter;
    let query = [];
    let status = [];
    _.each(filter, filter => {
      if (filter.parent === 1 || filter.parent === 2) {
        query.push(`operatorCode[]=${filter.value}`);
      } else {
        status.push(`isActive=${filter.value}`);
      };
    });
    if (status.length === 1) {
      query.push(status[0])
    };
    let searchQry = search.length > 0 ? `name%3D${search}` : '';
    props.getList(page, searchQry, encodeURIComponent(query.join('&')));
    setAppliedFilter(filter);
  }

  const toggleActive = (item, i) => {
    Swal.fire({
      title: `Are you sure you want to ${item.isActive ? 'deactivate' : 'activate'} the status of this product?`,
      confirmButtonText: item.isActive ? 'Deactivate' : 'Activate',
      cancelButtonText: 'Cancel',
      showCancelButton: true,
      reverseButtons: !item.isActive,
      focusConfirm: false,
      focusCancel: false,
      customClass: {
        confirmButton: `confirm-${item.isActive ? 'deactivate' : 'activate'}-btn`,
        cancelButton: `cancel-${item.isActive ? 'deactivate' : 'activate'}-btn`,
      }
    }).then(result => {
      if (result.dismiss) return;
      props.update(item.id, { isActive: !item.isActive });
    });
  };

  const changePage = page => {
    applyFilter(page);
  };

  const searchProduct = (e) => {
    if (!e || !e.keyCode || e.keyCode === 13) {
      applyFilter(1);
    }
  };

  const openEditModal = (item = {name: '', code: '', sellPrice: 0, adminPrice: 0}) => {
    setEditData(item);
    setShowEditModal(true);
  };

  const handleChange = (type, value) => {
    let items = {...editData};
    items[type] = value;
    setEditData(items);

    switch (type) {
      case 'name':
        setNameError(value.length <= 3)
        break;
      case 'code':
        setCodeError(value.length <= 3);
        break;
      default:
        break;
    }
  }

  const submitForm = () => {
    const { id, name, code, sellPrice, adminPrice } = editData;
    let body = {
      name, code, sellPrice, adminPrice
    }
    
    props.update(id, body).then(result => {
      if (_.get(result, 'payload.data.status', '') === 'success') {
        setShowEditModal(false);
        setEditData({name: '', code: '', sellPrice: 0});
        Swal.fire('', 'Product successfully updated').then(response => {
          props.getList(meta.page);
        })
      } else {
        Swal.fire('Error', '', 'error');
      };
    });
  }

  const syncData = () => {
    setIsSyncing(true);

    props.sync().then(result => {
      setIsSyncing(false);
      if (result.payload.data.postpaid || result.payload.data.prepaid) {
        Swal.fire('', 'Success', 'success').then(response => {
          props.getList(meta.page);
        })
      } else {
        Swal.fire('Error', '', 'error');
      };
    })
  }

  return (
    <Page>
      <div className="container-content container-recharges">
        <Breadcrumb>
          <Breadcrumb.Section>Recharges</Breadcrumb.Section>
          <Breadcrumb.Divider icon='right angle' />
          <Breadcrumb.Section active>Recharges List</Breadcrumb.Section>
        </Breadcrumb>

        <Grid>
          <Grid.Row columns={2} style={{marginTop: '24px'}}>
            <Grid.Column>
              <h2>Recharges List</h2>
            </Grid.Column>
            <Grid.Column>
              <Input
                action={{
                  color: 'teal',
                  icon: 'search',
                  onClick: searchProduct
                }}
                placeholder="Search Product Name or Product Code here..."
                onSubmit={searchProduct}
                fluid
                onChange={(e, {value}) => setSearch(value)}
                onKeyUp={(e) => searchProduct(e)}
              />
            </Grid.Column>
          </Grid.Row>
        
          <Grid.Row as={Segment}>
            <Grid.Column width={10}>
              <Dropdown 
                selection 
                placeholder="Filter by" 
                text="Filter by" 
                open={openFilter} 
                onClick={() => setOpenFilter(true)} 
                ref={filterDropdown}
              >
                <Dropdown.Menu style={{overflow: 'visible'}}>
                  {filter.map((item, i) => (
                    <Dropdown.Item 
                      key={i} 
                      onMouseEnter={() => setHoveredFilter(item.key)}
                      style={{fontWeight: _.find(item.children, child => child.checked, false) ? 'bold' : 'normal'}}
                    >
                      {item.title}

                      <div className="payment-filter-children" style={{display: hoveredFilter === item.key ? 'block' : 'none'}}>
                        {item.children.map((child, j) => (
                          <div key={j} style={{padding: '12px 0'}}>
                            <Checkbox 
                              label={child.text} 
                              checked={child.checked} 
                              onChange={(e, {checked}) => toggleFilter(i, j, checked)} 
                              style={{fontWeight: child.checked ? 'bold' : 'normal'}}
                            />
                          </div>
                        ))}
                      </div>
                    </Dropdown.Item>
                  ))}
                </Dropdown.Menu>
              </Dropdown>
              <Button 
                basic 
                color="teal" 
                style={{marginLeft: '24px'}} 
                disabled={checkedFilter.length === 0}
                onClick={(e) => applyFilter(1)}
              >Filter</Button>
              <span style={{color: '#46A997', fontWeight: 'bold', marginLeft: '24px', cursor: 'pointer'}} onClick={resetFilter}>Reset Filter</span>
            </Grid.Column>
            <Grid.Column width={6} textAlign="right">
              <Button basic color="black" style={{fontWeight: 'bold'}} onClick={syncData} disabled={isSyncing}>
                <Icon loading={isSyncing} name='sync' />&nbsp;&nbsp;
                Sync
              </Button>
            </Grid.Column>
          </Grid.Row>

          {appliedFilter.length > 0 && 
            <Grid.Row style={{padding: '0'}}>
              <Grid.Column style={{display: 'flex'}}>
                {appliedFilter.map((fi, i) => (
                  <div key={i} className="payment-filter-item">
                    {fi.text}
                    <Icon circular name="close" size="small" onClick={() => toggleFilter(fi.parent, fi.index, false, 'remove')} />
                  </div>    
                ))}
              </Grid.Column>
            </Grid.Row>
          }

          <Grid.Row as={Segment} style={{marginBottom: '30px', padding: '15px'}}>
            <Grid.Column>
              <Grid.Row style={{overflowX: 'scroll'}}>
                <Grid.Column>
                  <Table basic="very" unstackable fixed>
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell width={7}>Product Name & Product Code</Table.HeaderCell>
                        <Table.HeaderCell>Selling Price</Table.HeaderCell>
                        <Table.HeaderCell>Admin Price</Table.HeaderCell>
                        <Table.HeaderCell>Status</Table.HeaderCell>
                        <Table.HeaderCell></Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                    <Table.Body>
                      {isFetching ? 
                        <Table.Row style={{height: '400px'}}>
                          <Table.Cell textAlign="center" colSpan={5}>
                            Loading...
                          </Table.Cell>
                        </Table.Row>
                      : data.length > 0 ?
                        data.map((item, i) => (
                          <Table.Row key={i} verticalAlign='top'>
                            <Table.Cell width={7}>{item.name} <br/> ({item.code})</Table.Cell>
                            <Table.Cell style={{color: '#46A997', fontWeight: 'bold'}}><NumberFormat value={item.sellPrice} displayType="text" thousandSeparator={true} prefix="Rp " /></Table.Cell>
                            <Table.Cell style={{color: '#46A997', fontWeight: 'bold'}}><NumberFormat value={item.adminPrice} displayType="text" thousandSeparator={true} prefix="Rp " /></Table.Cell>
                            <Table.Cell>
                              <Checkbox
                                toggle 
                                checked={item.isActive} 
                                label={item.isActive ? 'Active' : 'Non Active'}
                                onChange={() => toggleActive(item, i)}
                              />
                            </Table.Cell>
                            <Table.Cell>
                              <Button basic fluid onClick={() => openEditModal(item)}>EDIT</Button>
                            </Table.Cell>
                          </Table.Row>
                        ))
                      :
                        <Table.Row style={{height: '400px'}}>
                          <Table.Cell textAlign="center" colSpan={5}>
                            Product Not Found
                          </Table.Cell>
                        </Table.Row>
                      }
                    </Table.Body>
                  </Table>
                </Grid.Column>
              </Grid.Row>
            </Grid.Column>
          </Grid.Row>

          {data.length > 0 &&
            <Grid.Row textAlign="center">
              <Grid.Column>
                <Pagination
                  activePage={_.get(meta, 'page', 1)}
                  onPageChange={(e, { activePage }) => changePage(activePage)}
                  boundaryRange={0}
                  totalPages={meta.totalPage}
                  firstItem={{ content: <Icon name='angle double left' />, icon: true }}
                  lastItem={{ content: <Icon name='angle double right' />, icon: true }}
                  prevItem={{ content: <Icon name='angle left' />, icon: true }}
                  nextItem={{ content: <Icon name='angle right' />, icon: true }}
                />
                <span style={{margin: '0 24px'}}>
                  {((meta.page-1) * 10) + 1}
                  - 
                  {meta.page === meta.totalPage ? meta.count : ((meta.page-1) * 10) + meta.limit}</span>
                <span style={{fontWeight: 'bold'}}>From {meta.count} Entries</span>
              </Grid.Column>
            </Grid.Row>
          }
        </Grid>
      </div>

      <Modal 
        open={showEditModal} 
        onClose={() => setShowEditModal(false)} 
        size="tiny"
        className="container-admin"
      >
        <Modal.Header>
          EDIT RECHARGE
          <span style={{float: 'right', cursor: 'pointer', fontSize: '25px'}} onClick={() => setShowEditModal(false)}>&times;</span>
        </Modal.Header>
        <Modal.Content>
          <Form className="admin-form">
            <Form.Field style={{marginBottom: '24px'}}>
              <div style={{marginBottom: '4px'}}>Product Name</div>
              <Input 
                placeholder="Product Name" 
                value={editData.name}
                onChange={(e, {value}) => handleChange('name', value)} 
              />
              {nameError &&
                <div style={{marginTop: '4px', color: '#EC1E24', fontSize: '10px'}}>At least 3 characters</div>
              }
            </Form.Field>
            <Form.Field style={{marginBottom: '24px'}}>
              <div style={{marginBottom: '4px'}}>Product Code</div>
              <Input 
                placeholder="Product Code" 
                value={editData.code}
                onChange={(e, {value}) => handleChange('code', value)} 
              />
              {codeError &&
                <div style={{marginTop: '4px', color: '#EC1E24', fontSize: '10px'}}>At least 3 characters</div>
              }
            </Form.Field>
            <Form.Field style={{marginBottom: '24px'}}>
              <div style={{marginBottom: '4px'}}>Selling Price</div>
              <NumberFormat 
                displayType={'input'} 
                thousandSeparator={true} 
                prefix={'Rp'} 
                placeholder="Selling Price" 
                value={editData.sellPrice}
                onValueChange={(values) => handleChange('sellPrice', values.floatValue)} 
              />
            </Form.Field>
            <Form.Field style={{marginBottom: '24px'}}>
              <div style={{marginBottom: '4px'}}>Admin Price</div>
              <NumberFormat 
                displayType={'input'} 
                thousandSeparator={true} 
                prefix={'Rp'} 
                placeholder="Admin Price" 
                value={editData.adminPrice}
                onValueChange={(values) => handleChange('adminPrice', values.floatValue)} 
              />
            </Form.Field>
          </Form>
        </Modal.Content>
      
        <Modal.Actions>
          <Button 
            onClick={submitForm}
            color="teal"
            disabled={!editData.name || !editData.code}
          >
            SAVE CHANGE
          </Button>
        </Modal.Actions>
      </Modal>
    </Page>
  );
};

const mapStateToProps = (state) => {
  return {
    data: state.recharges.list,
    isFetching: state.recharges.is_fetching_list,
    meta: state.recharges.meta
  }
}

const mapDispatchToProps = (dispatch) => {
	return {
    update: (id, data) => dispatch(update(id, data)),
    getList: (page, search, status) => dispatch(getList(page, search, status)),
    sync: () => dispatch(sync()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Recharges);