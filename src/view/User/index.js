import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import Page from '../Page';
import { Breadcrumb, Input, Grid, Table, Button, Checkbox, Pagination, Icon } from 'semantic-ui-react';
import './User.scss'
import Swal from 'sweetalert2';
import { getList, update } from '../../actions/users';
import { Link } from 'react-router-dom';
import Loading from '../Loading';
import _ from 'lodash';

const User = (props) => {
  let { users, meta } = props;
  const [search, setSearch] = useState('');

  useEffect(() => {
    props.getList();
    // eslint-disable-next-line
  }, []);
  
  const searchUser = (e) => {
    if (!e || !e.keyCode || e.keyCode === 13) {
      let query = search.length > 0 ? `id,name,phone=${search}` : '';
      props.getList(meta.page, encodeURIComponent(query));
    }
  };

  const toggleBlockUser = (user, i) => {
    Swal.fire({
      title: `Are you sure you want to ${!user.active ? 'unblock' : 'block'} this user?`,
      confirmButtonText: !user.active ? 'unblock' : 'block',
      cancelButtonText: 'Cancel',
      showCancelButton: true,
      reverseButtons: !user.active,
      focusConfirm: false,
      focusCancel: false,
      customClass: {
        confirmButton: `confirm-${!user.active ? 'unblock' : 'block'}-btn`,
        cancelButton: `cancel-${!user.active ? 'unblock' : 'block'}-btn`,
      }
    }).then(result => {
      if (result.dismiss) return;
      user.active = !user.active;
      props.update(user).then(result => {
        props.getList()
      });
    });
  };

  const changePage = page => {
    props.getList(page);
  };

  return (
    <Page>
      <div className="container-content container-user">
        <Breadcrumb>
          <Breadcrumb.Section>User</Breadcrumb.Section>
          <Breadcrumb.Divider icon='right angle' />
          <Breadcrumb.Section active>User List</Breadcrumb.Section>
        </Breadcrumb>

        <Grid>
          <Grid.Row columns={2} style={{marginTop: '24px'}}>
            <Grid.Column>
              <h2>User List</h2>
            </Grid.Column>
            <Grid.Column>
              <Input
                action={{
                  color: 'teal',
                  icon: 'search',
                  onClick: searchUser
                }}
                placeholder="Search User ID, Nama Lengkap, or No HP here..."
                onSubmit={searchUser}
                fluid
                onChange={(e, {value}) => setSearch(value)}
                onKeyUp={(e) => searchUser(e)}
              />
            </Grid.Column>
          </Grid.Row>

          <Grid.Row>
            <Grid.Column>
              <Table>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>User ID</Table.HeaderCell>
                    <Table.HeaderCell>Full Name</Table.HeaderCell>
                    <Table.HeaderCell>Phone Number</Table.HeaderCell>
                    <Table.HeaderCell>Status</Table.HeaderCell>
                    <Table.HeaderCell></Table.HeaderCell>
                  </Table.Row>
                </Table.Header>

                <Table.Body>
                  {users.length > 0 ? 
                    users.map((user, i) => (
                      <Table.Row key={user.id}>
                        <Table.Cell>{user.id}</Table.Cell>
                        <Table.Cell>{user.name}</Table.Cell>
                        <Table.Cell>{user.phone || '-'}</Table.Cell>
                        <Table.Cell>
                          <Checkbox 
                            toggle 
                            checked={!user.active} 
                            label={!user.active ? 'Blocked' : 'Unblocked'}
                            onChange={() => toggleBlockUser(user, i)}
                          />
                        </Table.Cell>
                        <Table.Cell>
                          <Link to={`/user/${user.id}`}><Button fluid basic>View</Button></Link>
                        </Table.Cell>
                      </Table.Row>
                    ))
                  : props.isFetching ?
                    <Loading />
                  :
                    <Table.Row style={{height: '400px'}}>
                      <Table.Cell textAlign="center" colSpan={5}>
                        User not found
                      </Table.Cell>
                    </Table.Row>
                  }
                </Table.Body>
              </Table>
            </Grid.Column>
          </Grid.Row>
          
          {users.length > 0 &&
            <Grid.Row textAlign="center">
              <Grid.Column>
                <Pagination
                  activePage={_.get(meta, 'page', 1)}
                  onPageChange={(e, { activePage }) => changePage(activePage)}
                  boundaryRange={0}
                  totalPages={meta.totalPage}
                  firstItem={{ content: <Icon name='angle double left' />, icon: true }}
                  lastItem={{ content: <Icon name='angle double right' />, icon: true }}
                  prevItem={{ content: <Icon name='angle left' />, icon: true }}
                  nextItem={{ content: <Icon name='angle right' />, icon: true }}
                />
                <span style={{margin: '0 24px'}}>
                  {((meta.page-1) * 10) + 1}
                  - 
                  {meta.page === meta.totalPage ? meta.count : ((meta.page-1) * 10) + meta.limit}</span>
                <span style={{fontWeight: 'bold'}}>From {meta.count} Entries</span>
              </Grid.Column>
            </Grid.Row>
          }
        </Grid>
      </div>
    </Page>
  );
};

const mapStateToProps = (state) => {
  return {
    users: state.users.list,
    isFetching: state.users.is_fetching_list,
    meta: state.users.meta
  }
}

const mapDispatchToProps = (dispatch) => {
	return {
    update: (item) => dispatch(update(item)),
    getList: (page, search) => dispatch(getList(page, search)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(User);