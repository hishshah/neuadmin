import React from 'react';
import NumberFormat from 'react-number-format';
import { Grid, Modal } from 'semantic-ui-react';
import _ from 'lodash';
import moment from 'moment';

const OrderDetailModal = ({ openModal, closeModal, item, calculateTotal }) => {
  return (
    <Modal open={openModal} onClose={() => closeModal(false)} size="tiny" className="payment-detail">
      <Modal.Header>
        Order Detail
        <span style={{float: 'right', cursor: 'pointer', fontSize: '25px'}} onClick={() => closeModal(false)}>&times;</span>
      </Modal.Header>
      <Modal.Content>
        <Grid>
          <Grid.Row>
            <Grid.Column width={5}>User ID</Grid.Column>
            <Grid.Column width={11}>: {_.get(item, 'User.id', '-')}</Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={5}>Full Name</Grid.Column>
            <Grid.Column width={11}>: {_.get(item, 'User.name', '-')}</Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={5}>Phone Number</Grid.Column>
            <Grid.Column width={11}>: {_.get(item, 'User.phone', '-')}</Grid.Column>
          </Grid.Row>
          <Grid.Row><Grid.Column></Grid.Column></Grid.Row>
          <Grid.Row>
            <Grid.Column width={5}>Order ID</Grid.Column>
            <Grid.Column width={11}>: <strong>{_.get(item, 'id', '-')}</strong></Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={5}>Product Detail</Grid.Column>
            <Grid.Column width={11}>: {_.get(item, 'Payment.thirdPartyResponse.bank_type', '-')}</Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={5}>Total Bill/Transfer</Grid.Column>
            <Grid.Column width={11}>: <NumberFormat value={calculateTotal(item.Payment)} displayType={'text'} thousandSeparator={true} prefix={'Rp'} /></Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={5}>Order Created</Grid.Column>
            <Grid.Column width={11}>: {item.createdAt ? moment(item.createdAt).format('DD MMMM YYYY (HH:mm)') : '-'}</Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={5}>Payment Confirmed</Grid.Column>
            <Grid.Column width={11}>: {_.get(item, 'Payment.confirmedAt') ? moment(item.confirmedAt).format('DD MMMM YYYY (HH:mm)') : '-'}</Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={5}>Order Expired</Grid.Column>
            <Grid.Column width={11}>: {_.get(item, 'Payment.expiredAt', false) ? moment(item.Payment.expiredAt).format('DD MMMM YYYY (HH:mm)') : '-'}</Grid.Column>
          </Grid.Row>
        </Grid>
      </Modal.Content>
    </Modal>
  );
};

export default OrderDetailModal;