import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Dropdown, Form, Grid, Modal, TextArea, Input, Button, Loader } from 'semantic-ui-react';
import NumberFormat from 'react-number-format';
import _ from 'lodash';
import moment from 'moment';
import { getBanks } from '../../actions/reference';
import { uploadFile } from '../../actions/upload';
import Swal from 'sweetalert2';
import { checkName, refundPayment } from '../../actions/payment';

const ManualRequestModal = (props) => {
  const { openModal, closeModal, item, openImageModal, onFinish, calculateTotal }  = props;
  const [banks, setBanks] = useState([]);
  const [bankQuery, setBankQuery] = useState('');
  const [refundReason, setRefundReason] = useState('');
  const [selectedBank, setSelectedBank] = useState('');
  const [accountNumber, setAccountNumber] = useState('');
  const [accountName, setAccountName] = useState('')
  const [refundAmount, setRefundAmount] = useState('');
  const [refundImage, setRefundImage] = useState('');
  const [uploadedImage, setUploadedImage] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    getBankList();
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    const timeOutId = setTimeout(() => searchBank(bankQuery), 500);
    return () => clearTimeout(timeOutId);
    // eslint-disable-next-line
  }, [bankQuery]);

  const getBankList = (search = '') => {
    props.getBanks(search).then(result => {
      if (_.get(result, 'payload.data.status', '') === 'success') {
        let bank_list = _.get(result, 'payload.data.data.bank', []);
        setBanks(bank_list.map(bank => ({ key: bank.id, value: bank.code, text: bank.name })))
      };
    });
  }

  const onFileChange = (e) => {
    let file = _.get(e, 'target.files[0]', false);
    if (file) {
      let body = new FormData();
      body.append('file', file);
      props.uploadFile(body).then(result => {
        if (result.type === 'UPLOAD_FILE_FAIL' || _.get(result, 'payload.data.status', '') !== 'success') {
          Swal.fire('Error!', 'Upload gambar gagal', 'error');
        } else {
          setRefundImage(file);
          setUploadedImage(_.get(result, 'payload.data.data.Location', ''));
        }
      })
    };
  };

  const handleSubmit = () => {
    setIsLoading(true);
    let body = {
      billId: item.id.toString(),
      proofImageUrl: uploadedImage,
      reason: refundReason,
      amount: refundAmount,
      detail: {
        bankCode: selectedBank,
        accountName,
        accountNo: accountNumber
      }
    }

    props.refundPayment(body).then(result => {
      setIsLoading(false);
      if (_.get(result, 'payload.data.status', '') === 'success') {
        Swal.fire('', 'Manual refund request has been successfully saved.', 'success').then(res => {
          closeModal();
          onFinish();
        });
      } else {
        Swal.fire('Error', '', 'error');
      };
    });
  }

  const searchBank = (value) => {
    getBankList(`name=${value}`);
  };

  const handleCheckName = () => {
    if (!selectedBank) {
      Swal.fire('Error', 'Silakan pilih bank terlebih dahulu', 'error');
      return;
    }

    if (!accountNumber) {
      Swal.fire('Error', 'Silakan isi Account Number terlebih dahulu', 'error');
      return;
    }

    let body = {
      userId: props.user_id,
      bankCode: selectedBank,
      accountNo: accountNumber
    }

    props.checkName(body).then(result => {
      if (!_.get(result, 'payload.data.data.account_name', false)) {
        Swal.fire('Error!', 'Nomor rekening tidak ditemukan', 'error');
      } else {
        setAccountName(result.payload.data.data.account_name);
      }
    })
  }
  
  return (
    <Modal open={openModal} onClose={() => closeModal()} className="payment-detail container-payment">
      <Modal.Header>
        Manual Request
        <span style={{float: 'right', cursor: 'pointer', fontSize: '25px'}} onClick={() => closeModal()}>&times;</span>
      </Modal.Header>
      <Modal.Content style={{padding: '20px 10px'}}>
        <Grid>
          <Grid.Row>
            <Grid.Column width={8} style={{padding: '0'}}>
              <Grid>
                <Grid.Row>
                  <Grid.Column width={6}>User ID</Grid.Column>
                  <Grid.Column width={10}>: {_.get(item, 'User.id', '-')}</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={6}>Full Name</Grid.Column>
                  <Grid.Column width={10}>: {_.get(item, 'User.name', '-')}</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={6}>Phone Number</Grid.Column>
                  <Grid.Column width={10}>: {_.get(item, 'User.phone', '-')}</Grid.Column>
                </Grid.Row>
              </Grid>
            </Grid.Column>
            <Grid.Column width={8}>
              <Grid>
                <Grid.Row>
                  <Grid.Column width={6}>Order ID</Grid.Column>
                  <Grid.Column width={10}>: <strong>{_.get(item, 'id', '-')}</strong></Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={6}>Product Detail</Grid.Column>
                  <Grid.Column width={10}>: {_.get(item, 'Payment.thirdPartyResponse.bank_type', '-')}</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={6}>Total Bill/Transfer</Grid.Column>
                  <Grid.Column width={10}>: <NumberFormat value={calculateTotal(item.Payment)} displayType={'text'} thousandSeparator={true} prefix={'Rp'} /></Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={6}>Order Created</Grid.Column>
                  <Grid.Column width={10}>: {item.createdAt ? moment(item.createdAt).format('DD MMMM YYYY (HH:mm)') : '-'}</Grid.Column>
                </Grid.Row>
              </Grid>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <h3>Form Refund</h3>
              <div style={{marginBottom: '8px'}}>Refund reason</div>
              <Form>
                <TextArea 
                  placeholder='Refund reason' 
                  style={{ minHeight: '100px', resize: 'none' }}  
                  value={refundReason}
                  onChange={(e, {value}) => setRefundReason(value)}
                  maxLength={500}
                />
              </Form>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row columns={3}>
            <Grid.Column>
              <div style={{marginBottom: '8px'}}>Select the destination bank</div>
              <Dropdown 
                selection 
                fluid
                placeholder="Select bank"
                search
                value={selectedBank}
                options={banks}
                onSearchChange={(e, { searchQuery }) => setBankQuery(searchQuery)}
                // onSearchChange={(e, { value }) => setBankQuery(value)}
                onChange={(e, { value }) => setSelectedBank(value)}
              />
            </Grid.Column>
            <Grid.Column>
              <div style={{marginBottom: '8px'}}>Account Number</div>
              <Input 
                fluid 
                action 
                placeholder='12345666781029300' 
                value={accountNumber} 
                onChange={(e, {value}) => setAccountNumber(value)} 
                type="number"
                className="account-number-check"
              >
                <input />
                <Button 
                  size="mini" 
                  color="teal" 
                  disabled={!accountNumber}
                  onClick={handleCheckName}
                >
                  Check
                </Button>
              </Input>
            </Grid.Column>
            <Grid.Column>
              <div style={{marginBottom: '8px'}}>&nbsp;</div>
              <Input style={{opacity: '1'}} fluid value={accountName}>
                <input 
                  disabled
                  style={{
                    backgroundColor: '#F2F4F5',
                    borderColor: '#E0E0E0',
                    opacity: '1'
                  }}
                />
              </Input>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row columns={3}>
            <Grid.Column>
              <div style={{marginBottom: '8px'}}>Refund Amount</div>
              <NumberFormat 
                customInput={Input}
                fluid
                placeholder="Enter the refund amount"
                value={refundAmount} 
                thousandSeparator={true} 
                onValueChange={(values) => setRefundAmount(values.value)}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row columns={3}>
            <Grid.Column>
              <div style={{marginBottom: '8px'}}>Image</div>
              <Input 
                placeholder=".jpeg"
                disabled
                readOnly 
                fluid
                value={_.get(refundImage, 'name', '')} 
              />
              <input 
                type="file" 
                name="file" 
                id="file" 
                className="input-file" 
                accept=".jpeg" 
                fluid 
                placeholder=".jpeg"
                onChange={(e) => onFileChange(e)}
              />
            </Grid.Column>
            <Grid.Column style={{padding: '0'}}>
              <div style={{marginBottom: '8px'}}>&nbsp;</div>
              {_.get(refundImage, 'name', false) &&
                <Button 
                  basic 
                  color="black"
                  style={{marginRight: '15px', width: '100px'}}
                  onClick={() => openImageModal(URL.createObjectURL(refundImage))}
                >
                  View
                </Button>
              }
              <label htmlFor="file" className="ui button basic black">Choose File</label>
            </Grid.Column>
          </Grid.Row>

          <Grid.Row textAlign="center" style={{marginTop: '70px'}}>
            <Grid.Column>
              <Button 
                color="teal" 
                style={{backgroundColor: '#46A997', width: '200px'}}
                onClick={handleSubmit}
                disabled={isLoading || !refundReason || !accountName || !accountNumber || !selectedBank || !refundAmount || !uploadedImage}
              >
                {isLoading ? <Loader active inline size="mini" /> : 'SUBMIT'}
              </Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Modal.Content>
    </Modal>
  );
};

const mapStateToProps = state => {
  return {
    user_id: _.get(state, 'auth.auth.user.id', '')
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getBanks: (search) => dispatch(getBanks(search)),
    uploadFile: (data) => dispatch(uploadFile(data)),
    checkName: (data) => dispatch(checkName(data)),
    refundPayment: (data) => dispatch(refundPayment(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManualRequestModal);