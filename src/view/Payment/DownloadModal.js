import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Input, Loader, Modal, Select } from 'semantic-ui-react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Swal from 'sweetalert2';
import _ from 'lodash';
import { downloadPayment } from '../../actions/payment';
import { downloadOrder } from '../../actions/order';

const DownloadModal = (props) => {
  const { openModal, closeModal, type } = props;
  const [dateSelection, setDateSelection] = useState('manual');
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const time_period = [
    { key: 'manual', value: 'manual', text: 'Select manual date' },
    { key: 'today', value: 'today', text: 'Today' },
    { key: 'week', value: 'week', text: 'This week' },
    { key: 'month', value: 'month', text: 'This month' },
  ];
  let data = [];
  const [isLoading, setIsLoading] = useState(false);

  const DatePickerInput = ({ value, onClick }) => (
    <Input 
      readonly 
      value={value} 
      onClick={onClick} 
      icon='calendar alternate outline'
      iconPosition='left'
      disabled={dateSelection !== 'manual'}
      className="payment-datepicker"
    />
  );

  const changeDateSelection = (value) => {
    let start, end;
    switch (value) {
      case 'today':
        start = moment().startOf('day');
        end = moment().endOf('day');
        break;
      case 'week':
        start = moment().startOf('week');
        end = moment().endOf('week');
        break;
      case 'month':
        start = moment().startOf('month');
        end = moment().endOf('month');
        break;
      default:
        break;
    }
    setDateSelection(value);

    if (value !== 'manual') {
      setStartDate(new Date(start));
      setEndDate(new Date(end));
    }
  };

  const calculateTotal = (payment) => {
    if (payment && payment.amount && payment.uniqueAmount) return parseInt(payment.amount, 10) + parseInt(payment.uniqueAmount, 10)

    return 0;
  };

  const processDownload =  () => {
    let start = moment(startDate).startOf('day').utc().format();
    let end = moment(endDate).endOf('day').utc().format();
    
    if (moment(start).isSameOrAfter(end)) {
      Swal.fire('Error', 'Start date must be before than end date', 'error');
      return;
    }

    if (type === 'payment') {
      getDataPayment(1, start, end)
    } else {
      getDataOrder(1, start, end)
    };
    setIsLoading(true);
  }

  const getDataPayment = (page, start, end) => {
    let query = `>:createdAt=${start}&<:createdAt=${end}`;

    props.getDataPayment(page, encodeURIComponent(query)).then(result => {
      if (_.get(result, 'payload.data.status', '') === 'success') {
        let results = _.get(result, 'payload.data.data', {});
        data.push(...results.bill);
        if (results.page < results.totalPage) {
          getDataPayment(page + 1, start, end);
        } else {
          if (data.length === 0) {
            setIsLoading(false);
            Swal.fire('', 'No data found', 'error');
          } else {
            let rows = [
              [
                'Ref Id', 
                'User Id', 
                'Payment Method', 
                'Payment Status', 
                'In Amount', 
                'Out Amount', 
                'Order Type', 
                'Order Status', 
                'Unique Code', 
                'Admin Fee', 
                'In Bank Abbrev',
                'In Account Name',
                'In Account Number',
                'Out Bank Code',
                'Out Account Name',
                'Out Account Number',
                'Created At',
                'Updated At',
                'Expired At',
                'Payment Confirmed At',
                'Refund Status',
                'Refund Bank Code',
                'Refund Account Name',
                'Refund Account Number',
                'Refund Amount',
                'Refund Transfer Receipts',
                'Refund Reason',
                'Refund Rejection Note',
                'Refund Created At',
                'Refund Updated At',
                'Refund Processed At'
              ]
            ];
        
            _.each(data, item => {
              let refund_status = _.get(item, 'refund.status', '');
              let refund_processed_at = refund_status === 'paid' ? _.get(item, 'refund.paidAt', '') : refund_status === 'rejected' ? _.get(item, 'refund.rejectedAt') : _.get(item, 'refund.updatedAt');

              rows.push([
                _.get(item, 'referenceId', ''), 
                _.get(item, 'User.id', ''),
                _.get(item, 'type', ''), 
                _.get(item, 'Payment.status', ''), 
                calculateTotal(item.Payment), 
                _.get(item, 'amount', 0), 
                _.get(item, 'type', ''), 
                _.get(item, 'status', ''), 
                _.get(item, 'Payment.uniqueAmount', ''), 
                _.get(item, 'Payment.adminFee', ''), 
                _.get(item, 'Payment.thirdPartyResponse.bank_type', ''),
                _.get(item, 'Payment.thirdPartyResponse.account_holder_name', ''), 
                _.get(item, 'Payment.thirdPartyResponse.account_no', ''), 
                _.get(item, 'detail.bankCode', ''), 
                _.get(item, 'detail.accountName', ''), 
                _.get(item, 'detail.accountNo', ''), 
                _.get(item, 'createdAt'),
                _.get(item, 'updatedAt'),
                _.get(item, 'Payment.expiredAt'),
                _.get(item, 'Payment.confirmedAt'),
                _.get(item, 'refund.status', ''),
                _.get(item, 'refund.detail.bankCode', ''),
                _.get(item, 'refund.detail.accountName', ''),
                _.get(item, 'refund.detail.accountNo', ''),
                _.get(item, 'refund.amount', ''),
                _.get(item, 'refund.proofImageUrl', ''),
                _.get(item, 'refund.reason', ''),
                _.get(item, 'refund.rejectedReason', ''),
                _.get(item, 'refund.createdAt', ''),
                _.get(item, 'refund.updatedAt', ''),
                refund_processed_at
              ]);
            })
            
            let csvContent = "data:text/csv;charset=utf-8," + rows.map(e => e.join(",")).join("\n");
            var encodedUri = encodeURI(csvContent);
            var link = document.createElement("a");
            link.setAttribute("href", encodedUri);
            link.setAttribute("download", `Payment_data_${moment(start).format('YYYY-MM-DD')}_${moment(end).format('YYYY-MM-DD')}.csv`);
            document.body.appendChild(link); // Required for FF
        
            link.click();
            setIsLoading(false);
          }
        }
      } else {
        Swal.fire('Error', 'Error fetching data', 'error');
      };
    });
  }
  
  const getDataOrder = (page, start, end) => {
    let query = `>:createdAt=${start}&<:createdAt=${end}`;

    props.getDataOrder(page, encodeURIComponent(query)).then(result => {
      if (_.get(result, 'payload.data.status', '') === 'success') {
        let results = _.get(result, 'payload.data.data', {});
        data.push(...results.bill);
        if (results.page < results.totalPage) {
          getDataOrder(page + 1, start, end);
        } else {
          if (data.length === 0) {
            setIsLoading(false);
            Swal.fire('', 'No data found', 'error');
          } else {
            let rows = [
              [
                'user_id', 
                'ref_id', 
                'username',
                'destination_no',
                'wallet_ref_id',
                'amount',
                'fee',
                'status',
                'recharge_type',
                'base_price',
                'admin_fee',
                'code',
                'name',
                'operator_code',
                'type',
                'user_sell_price',
                'user_admin_fee',
                'created_at'
              ]
            ];
        
            _.each(data, item => {
              rows.push([
                _.get(item, 'User.id', ''),
                _.get(item, 'referenceId', ''), 
                _.get(item, 'User.name', ''), 
                _.get(item, 'billNo', ''), 
                _.get(item, 'referenceId', ''), 
                _.get(item, 'amount', ''), 
                _.get(item, 'Payment.thirdPartyResponse.payment.fees', ''), 
                _.get(item, 'status', ''), 
                _.get(item, 'Product.type', ''), 
                _.get(item, 'Product.sellPrice', ''), 
                _.get(item, 'Product.adminPrice', ''), 
                _.get(item, 'Product.code', ''), 
                _.get(item, 'Product.name', ''), 
                _.get(item, 'Product.operatorCode', ''), 
                _.get(item, 'Product.productType', ''), 
                _.get(item, 'Payment.amount', ''), 
                _.get(item, 'Payment.adminFee', ''),
                _.get(item, 'createdAt', ''), 
              ]);
            })
            
            let csvContent = "data:text/csv;charset=utf-8," + rows.map(e => e.join(",")).join("\n");
            var encodedUri = encodeURI(csvContent);
            var link = document.createElement("a");
            link.setAttribute("href", encodedUri);
            link.setAttribute("download", `Order_data_${moment(start).format('YYYY-MM-DD.HH.mm')}_${moment(end).format('YYYY-MM-DD.HH.mm')}.csv`);
            document.body.appendChild(link); // Required for FF
        
            link.click();
            setIsLoading(false);
          }
        }
      } else {
        Swal.fire('Error', 'Error fetching data', 'error');
      };
    });
  }

  return (
    <Modal open={openModal} onClose={() => closeModal()} size="small">
      <Modal.Header>
        Download CSV Payment List
        <span style={{float: 'right', cursor: 'pointer', fontSize: '25px'}} onClick={() => closeModal()}>&times;</span>
      </Modal.Header>
      <Modal.Content>
        <div style={{marginBottom: '5px'}}>Select time period:</div>
        <Select 
          placeholder='Select time period' 
          options={time_period} 
          onChange={(e, {value}) => changeDateSelection(value)} 
          value={dateSelection}
        />

        <Grid>
          <Grid.Row columns={2} style={{margin: '24px 0'}}>
            <Grid.Column>
              Start
              <DatePicker 
                selected={startDate} 
                onChange={date => setStartDate(date)} 
                customInput={<DatePickerInput />}
                dateFormat="dd MMM yyyy"
                disabled={dateSelection !== 'manual'}
              />
            </Grid.Column>
            <Grid.Column>
              End
              <DatePicker 
                selected={endDate} 
                onChange={date => setEndDate(date)} 
                customInput={<DatePickerInput />}
                dateFormat="dd MMM yyyy"
                disabled={dateSelection !== 'manual'}
              />
            </Grid.Column>
          </Grid.Row>

          <Grid.Row textAlign="center">
            <Grid.Column>
              <Button 
                color="teal" 
                style={{width: '200px', backgroundColor: '#46A997'}} 
                onClick={processDownload} 
                disabled={isLoading || !startDate || !endDate}
              >
                {isLoading ? <Loader active inline size="mini" /> : 'DOWNLOAD'}
              </Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Modal.Content>
    </Modal>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    getDataPayment: (page, query) => dispatch(downloadPayment(page, query)),
    getDataOrder: (page, query) => dispatch(downloadOrder(page, query)),
  }
}

export default connect(null, mapDispatchToProps)(DownloadModal);