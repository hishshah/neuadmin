import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Button, Card, Dropdown, Form, Grid, Image, Label, Loader, Modal, TextArea } from 'semantic-ui-react';
import _ from 'lodash';
import NumberFormat from 'react-number-format';
import moment from 'moment';
import defaultImage from '../../assets/image.png';
import { updateStatus } from '../../actions/payment';
import Swal from 'sweetalert2';

const RefundDetailModal = (props) => {
  const { openModal, closeModal, item, openImageModal, onFinish, calculateTotal } = props;
  const [isLoading, setIsLoading] = useState(false);
  const [selectedRefundStatus, setSelectedRefundStatus] = useState('');
  const [rejectedReason, setRejectedReason] = useState('');
  const refund_status = _.get(item, 'refund.status', '-');

  const handleSubmit = () => {
    if (!_.get(item, 'refund.id', false)) {
      Swal.fire('Error', 'ID not found', 'error');
      return;
    }

    setIsLoading(true);
    let body = {
      type: selectedRefundStatus
    }

    if (selectedRefundStatus === 'rejected') {
      body.rejectedReason = rejectedReason;
    }

    props.updateStatus(item.refund.id, body).then(result => {
      setIsLoading(false);
      if (_.get(result, 'payload.data.status', '') === 'success') {
        Swal.fire('', 'Saved changes', 'success').then(res => {
          closeModal();
          onFinish();
        });
      } else {
        Swal.fire('Error', _.get(result, 'error.response.data.message', ''), 'error');
      };
    });
  }

  return (
    <Modal open={openModal} onClose={() => closeModal()} className="payment-detail container-payment">
      <Modal.Header>
        Refund Request Detail
        <span style={{float: 'right', cursor: 'pointer', fontSize: '25px'}} onClick={() => closeModal()}>&times;</span>
      </Modal.Header>
      <Modal.Content style={{padding: '20px 10px'}}>
        <Grid>
          <Grid.Row>
            <Grid.Column width={11} style={{padding: '0'}}>
              <Grid>
                <Grid.Row>
                  <Grid.Column width={4}>User ID</Grid.Column>
                  <Grid.Column width={12}>: {_.get(item, 'User.id', '-')}</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={4}>Full Name</Grid.Column>
                  <Grid.Column width={12}>: {_.get(item, 'User.name', '-')}</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={4}>Phone Number</Grid.Column>
                  <Grid.Column width={12}>: {_.get(item, 'User.phone', '-')}</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={4}>Order ID</Grid.Column>
                  <Grid.Column width={12}>: <strong>{_.get(item, 'id', '-')}</strong></Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={4}>Product Detail</Grid.Column>
                  <Grid.Column width={12}>: {_.get(item, 'Payment.thirdPartyResponse.bank_type', '-')}</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={4}>Total Bill/Transfer</Grid.Column>
                  <Grid.Column width={12}>: <NumberFormat value={calculateTotal(item.Payment)} displayType={'text'} thousandSeparator={true} prefix={'Rp'} /></Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={4}>Order Created</Grid.Column>
                  <Grid.Column width={12}>: {item.createdAt ? moment(item.createdAt).format('DD MMMM YYYY (HH:mm)') : '-'}</Grid.Column>
                </Grid.Row>
                <Grid.Row><Grid.Column></Grid.Column></Grid.Row>
                <Grid.Row>
                  <Grid.Column width={4}>Refund Request</Grid.Column>
                  <Grid.Column width={12}>: {_.get(item, 'refund.createdAt', false) ? moment(item.refund.createdAt).format('DD MMMM YYYY (HH:mm)') : '-'}</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={4}>Refund Reason</Grid.Column>
                  <Grid.Column width={12}>: {_.get(item, 'refund.reason')}</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={4}>Refund to</Grid.Column>
                  <Grid.Column width={12}>
                    <div>: {_.get(item, 'refund.detail.bankCode', '')} - {_.get(item, 'refund.detail.accountNo', '')}</div>
                    <div style={{paddingLeft: '8px'}}>{_.get(item, 'refund.detail.accountName', '')}</div>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={4}>Refund Amount</Grid.Column>
                  <Grid.Column width={12}>: <NumberFormat value={_.get(item, 'refund.amount', '-')} displayType={'text'} thousandSeparator={true} prefix={'Rp'} /></Grid.Column>
                </Grid.Row>
              </Grid>
            </Grid.Column>
            <Grid.Column width={5}>
              <Card onClick={() => openImageModal(_.get(item, 'refund.proofImageUrl', defaultImage))}>
                <Image src={_.get(item, 'refund.proofImageUrl', defaultImage)} style={{height: '175px', objectFit: 'contain', background: '#fff'}} />
                <Card.Content extra>
                  Bukti Transfer Pengguna
                </Card.Content>
              </Card>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={3}>
              <div style={{marginBottom: '8px'}}>Current Status</div>
              <div>
                <Label 
                  circular
                  style={{cursor: 'pointer'}}
                  color={refund_status === 'processing' ? 'orange' :refund_status === 'paid' ? 'blue' : refund_status === 'rejected' ? 'red' : refund_status === 'manual request' ? 'black' : 'teal'}
                >
                  {refund_status || '-'}
                </Label>
              </div>
            </Grid.Column>
            {refund_status === 'requested' &&
              <Grid.Column width={10}>
                <div style={{marginBottom: '8px'}}>Change status refund to: </div>
                <div>
                <Dropdown 
                  selection 
                  placeholder="Select status refund"
                  onChange={(e, {value}) => setSelectedRefundStatus(value)}
                  options={[
                    { key: 1, text: 'Processing', value: 'processing' },
                    { key: 2, text: 'Rejected', value: 'rejected' },
                  ]}
                />
                </div>
              </Grid.Column>
            }
          </Grid.Row>
          {selectedRefundStatus === 'rejected' &&
            <Grid.Row>
              <Grid.Column>
                <div style={{marginBottom: '8px'}}>Rejected reason</div>
                <Form>
                  <TextArea 
                    placeholder='Reject reason' 
                    style={{ minHeight: '100px', resize: 'none' }}  
                    value={rejectedReason}
                    onChange={(e, {value}) => setRejectedReason(value)}
                  />
                </Form>
              </Grid.Column>
            </Grid.Row>
          }
          {refund_status === 'requested' && 
            <Grid.Row textAlign="center" style={{marginTop: '70px'}}>
              <Grid.Column>
                <Button 
                  color="teal" 
                  style={{backgroundColor: '#46A997', width: '200px'}}
                  onClick={handleSubmit}
                  disabled={isLoading || !selectedRefundStatus || (selectedRefundStatus === 'rejected' && !rejectedReason)}
                >
                  {isLoading ? <Loader active inline size="mini" /> : 'SAVE CHANGE'}
                </Button>
              </Grid.Column>
            </Grid.Row>
          }
        </Grid>
      </Modal.Content>
    </Modal>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    updateStatus: (id, data) => dispatch(updateStatus(id, data))
  }
}

export default connect(null, mapDispatchToProps)(RefundDetailModal);