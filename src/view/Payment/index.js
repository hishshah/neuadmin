import React, { useState, useRef, useEffect, useCallback } from 'react';
import { connect } from 'react-redux';
import Page from '../Page';
import { Breadcrumb, Grid, Input, Segment, Button, Table, Label, Modal, Dropdown, Checkbox, Icon, Image, Pagination } from 'semantic-ui-react';
import download from '../../assets/icon/download.svg';
import './Payment.scss'
import 'react-datepicker/dist/react-datepicker.css';
import 'react-datepicker/dist/react-datepicker-cssmodules.css';
import moment from 'moment';
import _ from 'lodash';
import defaultImage from '../../assets/image.png';
import NumberFormat from 'react-number-format';
import { getList } from '../../actions/payment';
import DownloadModal from './DownloadModal';
import OrderDetailModal from './OrderDetailModal';
import ManualRequestModal from './ManualRequestModal';
import RefundDetailModal from './RefundDetailModal';

const Payment = (props) => {
  const { payment, meta, is_fetching_list } = props;
  const [showDownloadModal, setShowDownloadModal] = useState(false);
  const [hoveredFilter, setHoveredFilter] = useState('');
  const [openFilter, setOpenFilter] = useState(false);
  const filterDropdown = useRef(null);
  const [filter, setFilter] = useState([
    {
      key: 'order',
      title: 'Order Status',
      children: [
        { value: 'created', text: 'Created', checked: false },
        { value: 'waiting', text: 'Waiting', checked: false },
        { value: 'canceled', text: 'Canceled', checked: false },
        { value: 'completed', text: 'Completed', checked: false },
      ]
    },
    {
      key: 'payment',
      title: 'Payment Status',
      children: [
        { value: 'paid', text: 'Paid', checked: false },
        { value: 'unpaid', text: 'Unpaid', checked: false },
        { value: 'canceled', text: 'Canceled', checked: false },
      ]
    },
    {
      key: 'refund',
      title: 'Refund Status',
      children: [
        { value: 'requested', text: 'Requested', checked: false },
        { value: 'processing', text: 'Processing', checked: false },
        { value: 'paid', text: 'Paid', checked: false },
        { value: 'rejected', text: 'Rejected', checked: false },
      ]
    },
    // {
    //   key: 'cash_in',
    //   title: 'Cash-in',
    //   children: [
    //     { value: '014', text: 'BCA', checked: false },
    //     { value: '009', text: 'BNI', checked: false },
    //     { value: '008', text: 'Mandiri', checked: false },
    //     { value: '002', text: 'BRI', checked: false },
    //   ]
    // },
    // {
    //   key: 'cash_out',
    //   title: 'Cash-out',
    //   children: [
    //     // { value: 'out_neubill', text: 'neuBill', checked: false },
    //     // { value: 'out_neugrow', text: 'neuGrow', checked: false },
    //     { value: '014', text: 'BCA', checked: false },
    //     { value: '002', text: 'BRI', checked: false },
    //     { value: '008', text: 'Mandiri', checked: false },
    //     { value: '009', text: 'BNI', checked: false },
    //     // { value: 'out_jenius', text: 'Jenius', checked: false },
    //     { value: '013', text: 'Permata', checked: false },
    //   ]
    // },
  ]);
  const [checkedFilter, setCheckedFilter] = useState([]);
  const [appliedFilter, setAppliedFilter] = useState([]);
  const [detail, setDetail] = useState({});
  const [showDetailModal, setShowDetailModal] = useState(false);
  const [refundDetail, setRefundDetail] = useState({});
  const [showRefundDetailModal, setShowRefundDetailModal] = useState(false);
  const [showImageModal, setShowImageModal] = useState(false);
  const [showManualRequestModal, setShowManualRequestModal] = useState(false);
  const [modalImage, setModalImage] = useState(null);
  const [search, setSearch] = useState('');

  useEffect(() => {
    props.getList();
    // eslint-disable-next-line
  }, []);

  const handleClickOutside = useCallback(e => {
    let dropdown = _.get(filterDropdown, 'current.ref.current.innerHTML', '');
    let target = _.get(e, 'target.innerHTML', '');
    if (!_.includes(dropdown, target)) {
      setOpenFilter(false);
      setHoveredFilter('');
    };
  }, []);

  useEffect(() => {
    window.addEventListener('mousedown', handleClickOutside);

    return () => {
      window.removeEventListener('mousedown', handleClickOutside);
    };
  }, [handleClickOutside]);

  const toggleFilter = (parentIndex, childIndex, value, type) => {
    let new_filter = [...filter];
    let item = new_filter[parentIndex].children[childIndex];
    item.checked = value;
    
    if (value) {
      setCheckedFilter([...checkedFilter, {...item, parent: parentIndex, index: childIndex}]);
    } else {
      let filter = _.filter(checkedFilter, checked => checked.value !== item.value)
      setCheckedFilter(filter);
      if (type === 'remove') applyFilter(1, filter);
    };

    setFilter(new_filter);
  };

  const applyFilter = (page = 1, filter) => {
    setOpenFilter(false);
    if (!filter) filter = checkedFilter;
    let query = [];
    _.each(filter, filter => {
      switch (filter.parent) {
        case 0:
          query.push(`status[]=${filter.value}`);
          break;
        case 1:
          query.push(`payment.status[]=${filter.value}`);
          break;
        case 2:
          query.push(`refund.status[]=${filter.value}`)
          break;
        // case 3:
        //   query.push(`Payment.thirdPartyResponse.bank_code[]=${filter.value}`);
        //   break;
        // case 4:
        //   query.push(`detail.bankCode[]=${filter.value}`);
        //   break;
      
        default:
          break;
      }
    });
    if (search.length > 0) {
      query.push(`id=${search}`);
    };
    // let searchQry = search.length > 0 ? `id%3D${search}` : '';
    props.getList(page, '', encodeURIComponent(query.join('&')));
    setAppliedFilter(filter);
  }

  const resetFilter = () => {
    let new_filter = [...filter];
    _.each(new_filter, item => {
      _.each(item.children, child => {
        child.checked = false;
      });
    });
    setFilter(new_filter);
    setCheckedFilter([]);
    setAppliedFilter([]);
    let query = search.length > 0 ? `id%3D${search}` : '';
    props.getList(1, '', query);
  };

  const openDetail = (item) => {
    setDetail(item);
    setShowDetailModal(true);
  };

  const openRefundDetail = (item) => {
    setRefundDetail(item);
    if (_.get(item, 'refund.status', 'manual request') === 'manual request') {
      setShowManualRequestModal(true);
    } else {
      setShowRefundDetailModal(true);
    };
  };

  const closeRefundDetailModal = () => {
    setRefundDetail({});
    setShowRefundDetailModal(false);
    setShowManualRequestModal(false);
  }

  const openImageModal = (img = defaultImage) => {
    setModalImage(img);
    setShowImageModal(true);
  };

  const changePage = page => {
    applyFilter(page);
  };

  const searchPayment = (e) => {
    if (!e || !e.keyCode || e.keyCode === 13) {
      applyFilter(1);
    }
  };

  const isExpired = (item) => {
    if (_.isNil(item)) return false;
    
    let expiredTime = item.expiredAt;
    let status = item.status;
    
    return moment(expiredTime).isSameOrBefore(moment()) && status !== 'paid';
  }

  const calculateTotal = (payment) => {
    if (payment && payment.amount && payment.uniqueAmount) return parseInt(payment.amount, 10) + parseInt(payment.uniqueAmount, 10)

    return 0;
  }

  return (
    <Page>
      <div className="container-content container-payment">
        <Breadcrumb>
          <Breadcrumb.Section>Payment</Breadcrumb.Section>
          <Breadcrumb.Divider icon='right angle' />
          <Breadcrumb.Section active>Payment List</Breadcrumb.Section>
        </Breadcrumb>

        <Grid>
          <Grid.Row columns={2} style={{marginTop: '24px'}}>
            <Grid.Column>
              <h2>Payment List</h2>
            </Grid.Column>
            <Grid.Column>
              <Input
                action={{
                  color: 'teal',
                  icon: 'search',
                  onClick: searchPayment
                }}
                placeholder="Search Order ID here..."
                onChange={(e, {value}) => setSearch(value)}
                onKeyUp={(e) => searchPayment(e)}
                fluid
              />
            </Grid.Column>
          </Grid.Row>
        
          <Grid.Row as={Segment}>
            <Grid.Column width={10}>
              <Dropdown 
                selection 
                placeholder="Filter by" 
                text="Filter by" 
                open={openFilter} 
                onClick={() => setOpenFilter(true)} 
                ref={filterDropdown}
              >
                <Dropdown.Menu style={{overflow: 'visible'}}>
                  {filter.map((item, i) => (
                    <Dropdown.Item 
                      key={i} 
                      onMouseEnter={() => setHoveredFilter(item.key)}
                      style={{fontWeight: _.find(item.children, child => child.checked, false) ? 'bold' : 'normal'}}
                    >
                      {item.title}

                      <div className="payment-filter-children" style={{display: hoveredFilter === item.key ? 'block' : 'none'}}>
                        {item.children.map((child, j) => (
                          <div key={j} style={{padding: '12px 0'}}>
                            <Checkbox 
                              label={child.text} 
                              checked={child.checked} 
                              onChange={(e, {checked}) => toggleFilter(i, j, checked)} 
                              style={{fontWeight: child.checked ? 'bold' : 'normal'}}
                            />
                          </div>
                        ))}
                      </div>
                    </Dropdown.Item>
                  ))}
                </Dropdown.Menu>
              </Dropdown>
              <Button 
                basic 
                color="teal" 
                style={{marginLeft: '24px'}} 
                disabled={checkedFilter.length === 0}
                onClick={(e) => applyFilter(1)}
              >Filter</Button>
              <span style={{color: '#46A997', fontWeight: 'bold', marginLeft: '24px', cursor: 'pointer'}} onClick={resetFilter}>Reset Filter</span>
            </Grid.Column>
            <Grid.Column width={6} textAlign="right">
              <Button basic color="black" style={{fontWeight: 'bold'}} onClick={() => setShowDownloadModal(true)}>
                <img src={download} alt="" style={{verticalAlign: 'text-bottom', marginRight: '12px'}} />
                Download CSV
              </Button>
            </Grid.Column>
          </Grid.Row>

          {appliedFilter.length > 0 && 
            <Grid.Row style={{padding: '0'}}>
              <Grid.Column style={{display: 'flex'}}>
                {checkedFilter.map((fi, i) => (
                  <div key={i} className="payment-filter-item">
                    {fi.text}
                    <Icon circular name="close" size="small" onClick={() => toggleFilter(fi.parent, fi.index, false, 'remove')} />
                  </div>    
                ))}
              </Grid.Column>
            </Grid.Row>
          }

          <Grid.Row as={Segment} style={{marginBottom: '30px', padding: '15px'}}>
            <Grid.Column>
              {/* <Grid.Row style={{marginBottom: '30px'}}>
                <Grid.Column style={{textAlign: 'right', color: '#46A997'}}>
                  <span>1 - 11</span>
                  <span style={{marginRight: '10px'}}> dari 40 </span>
                  <Button icon='chevron left' circular size="mini" color="teal" style={{marginRight: '10px', backgroundColor: '#46A997', padding: '5px'}} />
                  <Button icon='chevron right' circular size="mini" color="teal" style={{backgroundColor: '#46A997', padding: '5px'}} />
                </Grid.Column>
              </Grid.Row> */}
              <Grid.Row style={{overflowX: 'scroll'}}>
                <Grid.Column>
                  <Table basic="very" unstackable>
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell>Order ID</Table.HeaderCell>
                        <Table.HeaderCell>Order Date</Table.HeaderCell>
                        <Table.HeaderCell>Cash-in</Table.HeaderCell>
                        <Table.HeaderCell>Cash-out</Table.HeaderCell>
                        <Table.HeaderCell>Order Status</Table.HeaderCell>
                        <Table.HeaderCell>Payment Status</Table.HeaderCell>
                        <Table.HeaderCell>Refund Status</Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                    <Table.Body>
                      {payment.length > 0 ?
                        payment.map((item, i) => {
                          const refund_status = _.get(item, 'refund.status', 'manual request');
                          return (
                            <Table.Row key={i} verticalAlign='top'>
                              <Table.Cell>
                                <div>{item.id}</div>
                                <div style={{color: '#46A997', textDecoration: 'underline', marginTop: '24px', cursor: 'pointer'}} onClick={() => openDetail(item)}>Order Detail</div>
                                {isExpired(_.get(item, 'Payment')) && <div style={{fontStyle: 'italic', marginTop: '20px', color: '#DB5555'}}>Order expired</div>}
                              </Table.Cell>
                              <Table.Cell>{moment(item.createdAt).format('DD MMM YYYY (HH:mm)')}</Table.Cell>
                              <Table.Cell style={{fontSize: '12px'}}>
                                {_.get(item, 'Payment', false) ?
                                  <div>
                                    <strong style={{fontSize: '14px'}}><NumberFormat value={calculateTotal(item.Payment)} displayType={'text'} thousandSeparator={true} prefix={'Rp'} /></strong>
                                    <div style={{margin: '8px 0'}}>{_.get(item, 'Payment.thirdPartyResponse.bank_type')}</div>
                                    <div>{_.get(item, 'Payment.thirdPartyResponse.account_no')}</div>
                                    <div style={{marginTop: '8px'}}>{_.get(item, 'Payment.thirdPartyResponse.account_holder_name')}</div>
                                  </div>
                                :
                                  <div>-</div>
                                }
                              </Table.Cell>
                              <Table.Cell style={{fontSize: '12px'}}>
                                {_.get(item, 'detail', false) ?
                                  <div>
                                    <strong style={{fontSize: '14px'}}><NumberFormat value={item.amount} displayType={'text'} thousandSeparator={true} prefix={'Rp'} /></strong>
                                    <div style={{margin: '8px 0'}}>{_.get(item, 'detail.bankCode')}</div>
                                    <div>{_.get(item, 'detail.accountNo')}</div>
                                    <div style={{marginTop: '8px'}}>{_.get(item, 'detail.accountName')}</div>
                                  </div>
                                :
                                  <div>-</div>
                                }
                              </Table.Cell>
                              <Table.Cell>
                                <Label circular color={item.status === 'failed' || item.status === 'canceled' ? 'red' : item.status === 'created' || item.status === 'waiting' ? 'navy' : 'teal'}>{item.status}</Label>
                              </Table.Cell>
                              <Table.Cell>
                                <Label circular color={_.get(item, 'Payment.status', '') === 'failed' || _.get(item, 'Payment.status', '') === 'canceled' ? 'red' : _.get(item, 'Payment.status', '') === 'confirmed' ? 'orange' : _.get(item, 'Payment.status', '') === 'unpaid' || _.get(item, 'Payment.status', '') === 'waiting' ? 'navy' : 'teal'}>{_.get(item, 'Payment.status', '')}</Label>
                              </Table.Cell>
                              <Table.Cell>
                                <Label 
                                  basic 
                                  onClick={() => openRefundDetail(item)}
                                  style={{cursor: 'pointer'}}
                                  color={refund_status === 'processing' ? 'orange' : refund_status === 'paid' ? 'blue' : refund_status === 'rejected' ? 'red' : refund_status === 'manual request' ? 'black' : 'teal'}
                                >
                                  {refund_status}
                                </Label>
                              </Table.Cell>
                            </Table.Row>
                          )
                        })
                      : is_fetching_list ?
                        <Table.Row style={{height: '400px'}}>
                          <Table.Cell textAlign="center" colSpan={7}>
                            Loading...
                          </Table.Cell>
                        </Table.Row>
                      :
                        <Table.Row style={{height: '400px'}}>
                          <Table.Cell textAlign="center" colSpan={7}>
                            Payment not found
                          </Table.Cell>
                        </Table.Row>
                      }
                    </Table.Body>
                  </Table>
                </Grid.Column>
              </Grid.Row>
            </Grid.Column>
          </Grid.Row>
        
          {payment.length > 0 &&
            <Grid.Row textAlign="center">
              <Grid.Column>
                <Pagination
                  activePage={_.get(meta, 'page', 1)}
                  onPageChange={(e, { activePage }) => changePage(activePage)}
                  boundaryRange={0}
                  totalPages={meta.totalPage}
                  firstItem={{ content: <Icon name='angle double left' />, icon: true }}
                  lastItem={{ content: <Icon name='angle double right' />, icon: true }}
                  prevItem={{ content: <Icon name='angle left' />, icon: true }}
                  nextItem={{ content: <Icon name='angle right' />, icon: true }}
                />
                <span style={{margin: '0 24px'}}>
                  {((meta.page-1) * 10) + 1}
                  - 
                  {meta.page === meta.totalPage ? meta.count : ((meta.page-1) * 10) + meta.limit}</span>
                <span style={{fontWeight: 'bold'}}>From {meta.count} Entries</span>
              </Grid.Column>
            </Grid.Row>
          }
        </Grid>
      </div>

      <DownloadModal
        openModal={showDownloadModal}
        closeModal={() => setShowDownloadModal(false)}
        type="payment"
      />

      <OrderDetailModal
        openModal={showDetailModal}
        closeModal={() => setShowDetailModal(false)}
        item={detail}
        calculateTotal={(payment) => calculateTotal(payment)}
      />
    
      <RefundDetailModal
        openModal={showRefundDetailModal}
        closeModal={() => closeRefundDetailModal()}
        item={refundDetail}
        openImageModal={(img) => openImageModal(img)}
        onFinish={() => applyFilter()}
        calculateTotal={(payment) => calculateTotal(payment)}
      />

      <ManualRequestModal
        openModal={showManualRequestModal}
        closeModal={() => closeRefundDetailModal()}
        item={refundDetail}
        openImageModal={(img) => openImageModal(img)}
        onFinish={() => applyFilter()}
        calculateTotal={(payment) => calculateTotal(payment)}
      />

      <Modal 
        open={showImageModal} 
        onClose={() => setShowImageModal(false)} 
        style={{width: 'auto'}}
        closeOnDimmerClick={false} 
        closeIcon
        centered={false}
      >
        <Modal.Content image style={{padding: '0'}}>
          {!_.isNil(modalImage) &&
            <Image size='large' src={modalImage} />
          }
        </Modal.Content>
      </Modal>
    </Page>
  );
};

const mapStateToProps = state => {
  return {
    payment: state.payment.list,
    meta: state.payment.meta,
    is_fetching_list: state.payment.is_fetching_list
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getList: (page, search, query) => dispatch(getList(page, search, query)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Payment);