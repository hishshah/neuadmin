import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'
import { Image, Icon } from 'semantic-ui-react';
import _ from 'lodash';
import defaultImage from '../../assets/image.png';
import logo from '../../assets/logo/maqmur_logo.svg';
import signout from '../../assets/icon/signout.svg';
import { toggleSidebar } from '../../actions/sidebar';
import { GoogleLogout } from 'react-google-login';
import { logout } from '../../actions/auth';
import { GOOGLE_CLIENT_ID } from '../../global';

const Header = (props) => {
  const [showDropdown, setShowDropdown] = useState(false);
  const { toggleSidebar } = props;

  useEffect(() => {
    if (!props.auth.isAuthenticate) {
      props.history.push('/login');
    };
    // eslint-disable-next-line
  }, [])

  const logoutResponse = response => {
    props.logout();
    props.history.push('/login');
  }

  return (
    <div className="neu-header">
      <div className="header-logo">
        <div className="header-bars">
          <Icon 
            name="bars" 
            size="large"
            inverted
            onClick={() => toggleSidebar()}
            style={{margin: '0'}}
          />
        </div>
        <img src={logo} alt="logo" style={{marginLeft: '25px', height: '50px'}} />
      </div>
      <div className="header-item" onClick={() => setShowDropdown(!showDropdown)}>
        <Image src={defaultImage} size='mini' circular style={{width: '35px', height: '35px'}} />
        <div className="header-profile">
          <div className="header-name">{_.get(props, 'auth.user.name')}</div>
          {/* <div className="header-role">Product Ops</div> */}
          <div className="header-role">{_.get(props, 'auth.user.type')}</div>
        </div>
        <Icon name="angle down" />
        {showDropdown && 
          <div className="header-dropdown">
            <GoogleLogout
              clientId={GOOGLE_CLIENT_ID}
              buttonText="Logout"
              onLogoutSuccess={logoutResponse}
              onFailure={logoutResponse}
              icon={false}
              render={renderProps => (
                <button 
                  onClick={renderProps.onClick} 
                  disabled={renderProps.disabled}
                  className="header-dropdown-item"
                  style={{
                    background: 'none',
                    border: 'none',
                    width: '100%',
                    textAlign: 'left',
                    outline: 'none',
                    cursor: 'pointer'
                  }}
                >
                  <img src={signout} alt="signout" style={{marginRight: '20px'}} />
                  Sign Out
                </button>
              )}
            >
            </GoogleLogout>
            {/* <button 
              onClick={logoutResponse} 
              className="header-dropdown-item"
              style={{
                background: 'none',
                border: 'none',
                width: '100%',
                textAlign: 'left',
                outline: 'none',
                cursor: 'pointer'
              }}
            >
              <img src={signout} alt="signout" style={{marginRight: '20px'}} />
              Sign Out
            </button> */}
          </div>
        }
      </div>

    </div>
  );
};

const mapStateToProps = state => {
  return { 
    auth: state.auth.auth
  }
}

const mapDispatchToProps = dispatch => {
	return {
    toggleSidebar: () => dispatch(toggleSidebar()),
    logout: () => dispatch(logout())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Header));