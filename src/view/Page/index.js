import React, { useState } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Sidebar, Icon, List, Image } from 'semantic-ui-react';
import Header from './Header';
import './Page.scss';
import logo from '../../assets/logo/maqmur_logo.svg';
import { toggleSidebar } from '../../actions/sidebar';

const Page = (props) => {
  const { children, location, history, showSidebar } = props;
  let pathname = location.pathname.split('/');
  const [activeMenu, setActiveMenu] = useState(pathname[1] === '' ? 'user' : pathname[1]);
  const menus = [
    { id: 'user', title: 'User', allowed_roles: ['cs', 'aml', 'ops', 'finance'] },
    { id: 'payment', title: 'Payment', allowed_roles: ['cs', 'aml', 'ops', 'finance'] },
    { id: 'order', title: 'Order', allowed_roles: ['cs', 'aml', 'ops', 'finance'] },
    { id: 'recharges', title: 'Recharges', allowed_roles: ['cs', 'aml', 'ops', 'finance'] },
    // { id: 'finance', title: 'Finance', allowed_roles: ['cs', 'aml', 'ops', 'finance'] },
    { id: 'admin', title: 'Admin', allowed_roles: ['cs', 'aml', 'ops', 'finance'] },
  ]

  const onChangeMenu = (menu) => {
    setActiveMenu(menu);
    history.push('/' + menu)
  };

  return (
    <div>
      <Sidebar.Pushable className="neu-sidebar">
        <Sidebar
          as={List}
          animation='push'
          visible={showSidebar}
          width='wide'
        >
          <List.Item>
            <List.Content style={{padding: '24px 24px 12px'}}>
              <img src={logo} alt="logo" style={{width: '50px'}} />
              <Icon 
                name="chevron left" 
                inverted
                size="large"
                onClick={() => props.toggleSidebar()} 
                style={{ float: 'right', marginTop: '10px', cursor: 'pointer' }}
              />
            </List.Content>
          </List.Item>
          
          {menus.map((menu) => {
            // if (!menu.allowed_roles.includes('ops')) return;
            let image = require(`../../assets/icon/sidebar_${menu.id}${activeMenu === menu.id ? '_active' : ''}.svg`);

            return (
              <List.Item 
                key={menu.id}
                className={`sidebar-item${activeMenu === menu.id ? ' active' : ''}`} 
                onClick={() => onChangeMenu(menu.id)}
              >
                <Image src={image} />
                <List.Content className="sidebar-item-title">
                  {menu.title}
                </List.Content>
              </List.Item>
            )
          })}
        </Sidebar>

        <Header />
        
        <Sidebar.Pusher>
          {children}
        </Sidebar.Pusher>
      </Sidebar.Pushable>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    showSidebar: state.sidebar.show
  }
}

const mapDispatchToProps = dispatch => {
	return {
    toggleSidebar: () => dispatch(toggleSidebar()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Page));