import React, { useState, useRef, useEffect, useCallback } from 'react';
import { connect } from 'react-redux';
import Page from '../Page';
import { Breadcrumb, Grid, Input, Segment, Button, Table, Label, Dropdown, Checkbox, Icon, Pagination, Modal, Image } from 'semantic-ui-react';
import './Order.scss';
import _ from 'lodash';
import moment from 'moment';
import NumberFormat from 'react-number-format';
import { Link } from 'react-router-dom';
import { getList } from '../../actions/order';
import RefundDetailModal from '../Payment/RefundDetailModal';
import ManualRequestModal from '../Payment/ManualRequestModal';
import defaultImage from '../../assets/image.png';
import DownloadModal from '../Payment/DownloadModal';
import download from '../../assets/icon/download.svg';

const Order = (props) => {
  const [showDownloadModal, setShowDownloadModal] = useState(false);
  const [hoveredFilter, setHoveredFilter] = useState('');
  const [openFilter, setOpenFilter] = useState(false);
  const filterDropdown = useRef(null);
  const [filter, setFilter] = useState([
    {
      key: 'prepaid',
      title: 'Prepaid',
      children: [
        { value: 'telkomsel_data', text: 'Telkomsel', checked: false },
        { value: 'xl_data', text: 'XL', checked: false },
        { value: 'indosat_data', text: 'Indosat', checked: false },
        { value: 'three_data', text: 'Tri', checked: false },
        { value: 'smart', text: 'Smarfren', checked: false },
        { value: 'axis', text: 'Axis', checked: false },
        { value: 'pln_data', text: 'Token PLN', checked: false },
      ]
    },
    {
      key: 'postpaid',
      title: 'Postpaid',
      children: [
        { value: 'telkomsel_postpaid', text: 'Telkomsel', checked: false },
        { value: 'xl_postpaid', text: 'XL', checked: false },
        { value: 'indosat_postpaid', text: 'Indosat', checked: false },
        { value: 'three_postpaid', text: 'Tri', checked: false },
        { value: 'smartfren_postpaid', text: 'Smarfren', checked: false },
        { value: 'axis_postpaid', text: 'Axis', checked: false },
        { value: 'pln_postpaid', text: 'Tagihan PLN', checked: false },
      ]
    },
    {
      key: 'order',
      title: 'Order Status',
      children: [
        { value: 'created', text: 'Created', checked: false },
        { value: 'waiting', text: 'Waiting', checked: false },
        { value: 'canceled', text: 'Canceled', checked: false },
        { value: 'completed', text: 'Completed', checked: false },
      ]
    },
  ]);
  const [checkedFilter, setCheckedFilter] = useState([]);
  const [appliedFilter, setAppliedFilter] = useState([]);
  const [search, setSearch] = useState('');
  const [refundDetail, setRefundDetail] = useState({});
  const [showRefundDetailModal, setShowRefundDetailModal] = useState(false);
  const [showImageModal, setShowImageModal] = useState(false);
  const [showManualRequestModal, setShowManualRequestModal] = useState(false);
  const [modalImage, setModalImage] = useState(null);
  const { order, meta, is_fetching_list } = props;

  useEffect(() => {
    props.getList();
    // eslint-disable-next-line
  }, []);

  const handleClickOutside = useCallback(e => {
    let dropdown = _.get(filterDropdown, 'current.ref.current.innerHTML', '');
    let target = _.get(e, 'target.innerHTML', '');
    if (!_.includes(dropdown, target)) {
      setOpenFilter(false);
      setHoveredFilter('');
    };
  }, []);

  useEffect(() => {
    window.addEventListener('mousedown', handleClickOutside);

    return () => {
      window.removeEventListener('mousedown', handleClickOutside);
    };
  }, [handleClickOutside]);

  const toggleFilter = (parentIndex, childIndex, value, type) => {
    let new_filter = [...filter];
    let item = new_filter[parentIndex].children[childIndex];
    item.checked = value;
    
    if (value) {
      setCheckedFilter([...checkedFilter, {...item, parent: parentIndex, index: childIndex}]);
    } else {
      let filter = _.filter(checkedFilter, checked => checked.value !== item.value)
      setCheckedFilter(filter);
      if (type === 'remove') applyFilter(1, filter);
    };

    setFilter(new_filter);
  };

  const resetFilter = () => {
    let new_filter = [...filter];
    _.each(new_filter, item => {
      _.each(item.children, child => {
        child.checked = false;
      });
    });
    setFilter(new_filter);
    setCheckedFilter([]);
    setAppliedFilter([]);
    let query = search.length > 0 ? `paymentId%2CbillNo%3D${search}` : '';
    props.getList(1, query, '');
  };

  const applyFilter = (page = 1, filter) => {
    setOpenFilter(false);
    if (!filter) filter = checkedFilter;
    let query = [];
    _.each(filter, filter => {
      if (filter.parent === 0 || filter.parent === 1) {
        query.push(`productDetail.operatorCode[]=${filter.value}`);
      } else {
        query.push(`status[]=${filter.value}`);
      };
    });
    // if (search.length > 0) {
    //   query.push(`id=${search}`);
    // };
    let searchQry = search.length > 0 ? `paymentId%2CbillNo%3D${search}` : '';
    props.getList(page, searchQry, encodeURIComponent(query.join('&')));
    setAppliedFilter(filter);
  }

  const changePage = page => {
    applyFilter(page);
  };

  const searchOrder = (e) => {
    if (!e || !e.keyCode || e.keyCode === 13) {
      applyFilter(1);
    }
  };

  const openRefundDetail = (item) => {
    setRefundDetail(item);
    if (_.get(item, 'refund.status', 'manual request') === 'manual request') {
      setShowManualRequestModal(true);
    } else {
      setShowRefundDetailModal(true);
    };
  };

  const closeRefundDetailModal = () => {
    setRefundDetail({});
    setShowRefundDetailModal(false);
    setShowManualRequestModal(false);
  }

  const calculateTotal = (payment) => {
    if (payment && payment.amount && payment.uniqueAmount) return parseInt(payment.amount, 10) + parseInt(payment.uniqueAmount, 10)

    return 0;
  }

  const openImageModal = (img = defaultImage) => {
    setModalImage(img);
    setShowImageModal(true);
  };

  return (
    <Page>
      <div className="container-content container-order">
        <Breadcrumb>
          <Breadcrumb.Section>Order</Breadcrumb.Section>
          <Breadcrumb.Divider icon='right angle' />
          <Breadcrumb.Section active>Order List</Breadcrumb.Section>
        </Breadcrumb>

        <Grid>
          <Grid.Row columns={2} style={{marginTop: '24px'}}>
            <Grid.Column>
              <h2>Order List</h2>
            </Grid.Column>
            <Grid.Column>
              <Input
                action={{
                  color: 'teal',
                  icon: 'search',
                  onClick: searchOrder
                }}
                placeholder="Search Order ID, phone number here..."
                onChange={(e, {value}) => setSearch(value)}
                onKeyUp={(e) => searchOrder(e)}
                fluid
              />
            </Grid.Column>
          </Grid.Row>
        
          <Grid.Row as={Segment}>
            <Grid.Column width={10}>
              <Dropdown 
                selection 
                placeholder="Filter by" 
                text="Filter by" 
                open={openFilter} 
                onClick={() => setOpenFilter(true)} 
                ref={filterDropdown}
              >
                <Dropdown.Menu style={{overflow: 'visible'}}>
                  {filter.map((item, i) => (
                    <Dropdown.Item 
                      key={i} 
                      onMouseEnter={() => setHoveredFilter(item.key)}
                      style={{fontWeight: _.find(item.children, child => child.checked, false) ? 'bold' : 'normal'}}
                    >
                      {item.title}

                      <div className="payment-filter-children" style={{display: hoveredFilter === item.key ? 'block' : 'none'}}>
                        {item.children.map((child, j) => (
                          <div key={j} style={{padding: '12px 0'}}>
                            <Checkbox 
                              label={child.text} 
                              checked={child.checked} 
                              onChange={(e, {checked}) => toggleFilter(i, j, checked)} 
                              style={{fontWeight: child.checked ? 'bold' : 'normal'}}
                            />
                          </div>
                        ))}
                      </div>
                    </Dropdown.Item>
                  ))}
                </Dropdown.Menu>
              </Dropdown>
              <Button 
                basic 
                color="teal" 
                style={{marginLeft: '24px'}} 
                disabled={checkedFilter.length === 0}
                onClick={(e) => applyFilter(1)}
              >Filter</Button>
              <span style={{color: '#46A997', fontWeight: 'bold', marginLeft: '24px', cursor: 'pointer'}} onClick={resetFilter}>Reset Filter</span>
            </Grid.Column>
            <Grid.Column width={6} textAlign="right">
              <Button basic color="black" style={{fontWeight: 'bold'}} onClick={() => setShowDownloadModal(true)}>
                <img src={download} alt="" style={{verticalAlign: 'text-bottom', marginRight: '12px'}} />
                Download CSV
              </Button>
            </Grid.Column>
          </Grid.Row>

          {appliedFilter.length > 0 && 
            <Grid.Row style={{padding: '0'}}>
              <Grid.Column style={{display: 'flex'}}>
                {appliedFilter.map((fi, i) => (
                  <div key={i} className="payment-filter-item">
                    {fi.text}
                    <Icon circular name="close" size="small" onClick={() => toggleFilter(fi.parent, fi.index, false, 'remove')} />
                  </div>    
                ))}
              </Grid.Column>
            </Grid.Row>
          }

          <Grid.Row as={Segment} style={{marginBottom: '30px', padding: '15px'}}>
            <Grid.Column>
              <Grid.Row style={{overflowX: 'scroll'}}>
                <Grid.Column>
                  <Table basic="very" unstackable>
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell>Order ID</Table.HeaderCell>
                        <Table.HeaderCell>Order Date</Table.HeaderCell>
                        <Table.HeaderCell>Customer</Table.HeaderCell>
                        <Table.HeaderCell>Product</Table.HeaderCell>
                        <Table.HeaderCell>Order</Table.HeaderCell>
                        <Table.HeaderCell>Amount</Table.HeaderCell>
                        <Table.HeaderCell>Order Status</Table.HeaderCell>
                        <Table.HeaderCell>Refund Status</Table.HeaderCell>
                        <Table.Header>Action</Table.Header>
                      </Table.Row>
                    </Table.Header>
                    <Table.Body>
                      {order.length > 0 ?
                        order.map((item, i) => {
                          const refund_status = _.get(item, 'refund.status', 'manual request');
                          return (
                            <Table.Row key={i} verticalAlign='top'>
                              <Table.Cell>{item.id}</Table.Cell>
                              <Table.Cell>{moment(item.createdAt).format('DD MMM YYYY (HH:mm)')}</Table.Cell>
                              <Table.Cell>
                                <div style={{marginBottom: '8px'}}>
                                  <strong>Phone: </strong>
                                  <span>{_.get(item, 'User.phone', '-')}</span>
                                </div>
                                <div>
                                  <strong>Name: </strong>
                                  <span>{_.get(item, 'User.name', '-')}</span>
                                </div>
                              </Table.Cell>
                              <Table.Cell>{_.get(item, 'productDetail.name', '-')}</Table.Cell>
                              <Table.Cell>
                                <div style={{marginBottom: '8px'}}>
                                  <strong>Number: </strong>
                                  <span>{_.get(item, 'billNo', '-')}</span>
                                </div>
                                <div style={{marginBottom: '8px'}}>
                                  <strong>Amount: </strong>
                                  <span><NumberFormat value={_.get(item, 'amount', '-')} displayType={'text'} thousandSeparator={true} /></span>
                                </div>
                                <div>
                                  <strong>Description: </strong>
                                  <span>{_.get(item, 'note', '-') || '-'}</span>
                                </div>
                              </Table.Cell>
                              <Table.Cell style={{color: '#46A997'}}><NumberFormat value={_.get(item, 'amount', '-')} displayType={'text'} thousandSeparator={true} prefix={'Rp'} /></Table.Cell>
                              <Table.Cell>
                                <Label circular color={item.status === 'canceled' ? 'red' : item.status === 'completed' ? 'teal' : 'navy'}>{item.status}</Label>
                              </Table.Cell>
                              <Table.Cell>
                                <Label 
                                  basic 
                                  onClick={() => openRefundDetail(item)}
                                  style={{cursor: 'pointer'}}
                                  color={refund_status === 'processing' ? 'orange' : refund_status === 'paid' ? 'blue' : refund_status === 'rejected' ? 'red' : refund_status === 'manual request' ? 'black' : 'teal'}
                                >
                                  {refund_status}
                                </Label>
                              </Table.Cell>
                              <Table.Cell>
                                <Link to={`/order/${item.id}`}><Button basic fluid compact color="teal">View Detail</Button></Link>
                              </Table.Cell>
                            </Table.Row>
                          )
                        })
                      : is_fetching_list ?
                        <Table.Row style={{height: '400px'}}>
                          <Table.Cell textAlign="center" colSpan={7}>
                            Loading...
                          </Table.Cell>
                        </Table.Row>
                      :
                        <Table.Row style={{height: '400px'}}>
                          <Table.Cell textAlign="center" colSpan={7}>
                            Order not found
                          </Table.Cell>
                        </Table.Row>
                      }
                    </Table.Body>
                  </Table>
                </Grid.Column>
              </Grid.Row>
            </Grid.Column>
          </Grid.Row>

          {order.length > 0 &&
            <Grid.Row textAlign="center">
              <Grid.Column>
                <Pagination
                  activePage={_.get(meta, 'page', 1)}
                  onPageChange={(e, { activePage }) => changePage(activePage)}
                  boundaryRange={0}
                  totalPages={meta.totalPage}
                  firstItem={{ content: <Icon name='angle double left' />, icon: true }}
                  lastItem={{ content: <Icon name='angle double right' />, icon: true }}
                  prevItem={{ content: <Icon name='angle left' />, icon: true }}
                  nextItem={{ content: <Icon name='angle right' />, icon: true }}
                />
                <span style={{margin: '0 24px'}}>
                  {((meta.page-1) * 10) + 1}
                  - 
                  {meta.page === meta.totalPage ? meta.count : ((meta.page-1) * 10) + meta.limit}</span>
                <span style={{fontWeight: 'bold'}}>From {meta.count} Entries</span>
              </Grid.Column>
            </Grid.Row>
          }
        </Grid>
      </div>

      <RefundDetailModal
        openModal={showRefundDetailModal}
        closeModal={() => closeRefundDetailModal()}
        item={refundDetail}
        openImageModal={(img) => openImageModal(img)}
        onFinish={() => applyFilter()}
        calculateTotal={(payment) => calculateTotal(payment)}
      />

      <ManualRequestModal
        openModal={showManualRequestModal}
        closeModal={() => closeRefundDetailModal()}
        item={refundDetail}
        openImageModal={(img) => openImageModal(img)}
        onFinish={() => applyFilter()}
        calculateTotal={(payment) => calculateTotal(payment)}
      />

      <Modal 
        open={showImageModal} 
        onClose={() => setShowImageModal(false)} 
        style={{width: 'auto'}}
        closeOnDimmerClick={false} 
        closeIcon
        centered={false}
      >
        <Modal.Content image style={{padding: '0'}}>
          {!_.isNil(modalImage) &&
            <Image size='large' src={modalImage} />
          }
        </Modal.Content>
      </Modal>

      <DownloadModal
        openModal={showDownloadModal}
        closeModal={() => setShowDownloadModal(false)}
        type="order"
      />
    </Page>
  );
};

const mapStateToProps = state => {
  return {
    order: state.order.list,
    meta: state.order.meta,
    is_fetching_list: state.order.is_fetching_list
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getList: (page, search, query) => dispatch(getList(page, search, query)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Order);