import React from 'react';
import { connect } from 'react-redux';
import { Grid, Button } from 'semantic-ui-react';
import Swal from 'sweetalert2';
import moment from 'moment';
import { update } from '../../actions/users';

const UserProfile = (props) => {
  const { user } = props;

  const toggleBlockUser = () => {
    Swal.fire({
      title: `Are you sure you want to ${!user.active ? 'unblock' : 'block'} this user?`,
      confirmButtonText: !user.active ? 'unblock' : 'block',
      cancelButtonText: 'Cancel',
      showCancelButton: true,
      reverseButtons: !user.active,
      focusConfirm: false,
      focusCancel: false,
      customClass: {
        confirmButton: `confirm-${!user.active ? 'unblock' : 'block'}-btn`,
        cancelButton: `cancel-${!user.active ? 'unblock' : 'block'}-btn`,
      }
    }).then(result => {
      if (result.dismiss) return;
      user.active = !user.active;
      props.update(user).then(result => {
        props.getDetail()
      });
    });
  };

  return (
    <div className="user-detail-tab">
      <Grid columns={2}>
        <Grid.Row>
          <Grid.Column width={3}>User ID</Grid.Column>
          <Grid.Column width={9}>: {user.id}</Grid.Column>
        </Grid.Row>
        <Grid.Row style={{padding: '0'}}>
          <Grid.Column width={3}>Full Name</Grid.Column>
          <Grid.Column width={9}>: {user.name}</Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={3}>Phone Number</Grid.Column>
          <Grid.Column width={9}>: {user.phone || '-'}</Grid.Column>
        </Grid.Row>
      </Grid>

      <h5>Membership</h5>

      <Grid columns={2}>
        <Grid.Row>
          <Grid.Column width={3}>Member Since</Grid.Column>
          <Grid.Column width={9}>: {moment(user.createdAt).format('DD MMM YYYY (HH:mm)')}</Grid.Column>
        </Grid.Row>
        <Grid.Row className="user-status">
          <Grid.Column width={3}>Status</Grid.Column>
          <Grid.Column width={9}>
            : <Button color={!user.active ? "red" : "green"} basic>{user.active && 'Not ' }Blocked</Button>
            {/* <Button color="red" basic>Blocked</Button> */}
          </Grid.Column>
        </Grid.Row>
        <Grid.Row className="block-btn">
          <Grid.Column>
            <Button color={!user.active ? "orange" : "red"} onClick={toggleBlockUser}>
              {!user.active ? 'UNBLOCK' : 'BLOCK'}
            </Button>
          </Grid.Column>
        </Grid.Row>
      </Grid>

    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
	return {
    update: (item) => dispatch(update(item)),
  }
}

export default connect(null, mapDispatchToProps)(UserProfile);