import React from 'react';
import { Grid, Button, Card, Image, Modal } from 'semantic-ui-react';
import moment from 'moment';
import defaultImage from '../../assets/image.png';
import { useState } from 'react';
import _ from 'lodash';

const UserProfile = (props) => {
  const { user } = props;
  const [showUserModal, setShowUserModal] = useState(false);
  const [imageModal, setImageModal] = useState(defaultImage);
  let kyc_status = _.get(user, 'UserXfer.response.verification_pending', '') === 'true' ? 'Processed' : _.get(user, 'UserXfer.response.verification_rejected', '') === 'true' ? 'Rejected' : 'Verified';

  const openImageModal = image => {
    setShowUserModal(true);
    setImageModal(image);
  }

  return (
    <div className="user-detail-tab user-kyc">
      <Grid>
        {user && user.UserXfer ? 
          <Grid.Row>
            <Grid.Column width={9}>
              <h3>KYC Data</h3>
              <Grid columns={2} style={{marginTop: '24px'}}>
                <Grid.Row>
                  <Grid.Column width={5}>NIK</Grid.Column>
                  <Grid.Column width={9}>: {_.get(user, 'UserXfer.identity_no', '-')}</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={5}>Nama Lengkap</Grid.Column>
                  <Grid.Column width={9}>: {_.get(user, 'name', '-')}</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={5}>Tempat Lahir</Grid.Column>
                  <Grid.Column width={9}>: {_.get(user, 'UserXfer.place_of_birth', '-')}</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={5}>Tanggal Lahir</Grid.Column>
                  <Grid.Column width={9}>: {_.get(user, 'UserXfer.date_of_birth', false) ? moment(user.UserXfer.date_of_birth).format('DD MMMM YYYY') : '-'}</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={5}>Jenis Kelamin</Grid.Column>
                  <Grid.Column width={9}>: {_.get(user, 'UserXfer.gender', '-')}</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={5}>Alamat Sesuai KTP</Grid.Column>
                  <Grid.Column width={9}>: {_.get(user, 'UserXfer.address_line_1', '-')}</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={5}>Pekerjaan</Grid.Column>
                  <Grid.Column width={9}>: {_.get(user, 'UserXfer.occupation', '-')}</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={5}>Status Perkawinan</Grid.Column>
                  <Grid.Column width={9}>: {_.get(user, 'UserXfer.marital_status', '-')}</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={5}>Ibu Kandung</Grid.Column>
                  <Grid.Column width={9}>: {_.get(user, 'UserXfer.mother_maiden_name', '-')}</Grid.Column>
                </Grid.Row>
              </Grid>

              <h5>Verification Submission</h5>

              <Grid columns={2} style={{marginTop: '15px'}}>
                <Grid.Row>
                  <Grid.Column width={5}>Submission Date</Grid.Column>
                  <Grid.Column width={9}>: {_.get(user, 'UserXfer.createdAt', false) ? moment(user.UserXfer.createdAt).format('DD MMM YYYY (HH:mm)') : '-'}</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={5}>Last Update</Grid.Column>
                  <Grid.Column width={9}>: {_.get(user, 'UserXfer.updatedAt', false) ? moment(user.UserXfer.createdAt).format('DD MMM YYYY (HH:mm)') : '-'}</Grid.Column>
                </Grid.Row>
                <Grid.Row className="user-status">
                  <Grid.Column width={5}>Status</Grid.Column>
                  <Grid.Column width={9}>
                    : <Button color={kyc_status === 'Verified' ? "green" : kyc_status === 'Processed' ? "orange" : "red"} basic>{kyc_status}</Button>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Grid.Column>

            <Grid.Column width={7}>
              <h3>KYC Photo</h3>

              <Card onClick={() => openImageModal(_.get(user, 'UserXfer.id_front_url', defaultImage))}>
                <Image src={_.get(user, 'UserXfer.id_front_url', defaultImage)} style={{height: '175px', objectFit: 'contain', background: '#fff'}} />
                <Card.Content extra>
                  ID Card Photo
                </Card.Content>
              </Card>

              <Card style={{marginTop: '50px'}} onClick={() => openImageModal(_.get(user, 'UserXfer.selfie_2id_url', defaultImage))}>
                <Image src={_.get(user, 'UserXfer.selfie_2id_url', defaultImage)} style={{height: '175px', objectFit: 'contain', background: '#fff'}} />
                <Card.Content extra>
                  ID Card Photo with KTP
                </Card.Content>
              </Card>
            </Grid.Column>
          </Grid.Row>
        :
          <Grid.Row style={{height: '400px'}} textAlign="center" verticalAlign="middle">
            <Grid.Column>
              No KYC Detail Found
            </Grid.Column>
          </Grid.Row>
        }
      </Grid>
      
      <Modal 
        open={showUserModal} 
        onClose={() => setShowUserModal(false)} 
        style={{width: 'auto'}}
        closeOnDimmerClick={false} 
        closeIcon
      >
        <Modal.Content image style={{padding: '0'}}>
          <Image size='large' src={imageModal} />
        </Modal.Content>
      </Modal>
    </div>
  );
};

export default UserProfile;