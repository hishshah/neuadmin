import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import Page from '../Page';
import { Breadcrumb, Segment, Tab } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import './UserDetail.scss'
import UserProfile from './UserProfile';
// import UserEvent from './UserEvent';
import UserKYC from './UserKYC';
import { getDetail } from '../../actions/users';

const UserDetail = (props) => {
  const panes = [
    {
      menuItem: 'Profile',
      render: () => <UserProfile user={props.user} getDetail={() => props.getDetail(id)} />,
    },
    // {
    //   menuItem: 'User Event',
    //   render: () => <UserEvent user={props.user} />,
    // },
    {
      menuItem: 'KYC Detail',
      render: () => <UserKYC user={props.user} />,
    },
  ]
  const [id, setID] = useState(props.match.params.id);

  useEffect(() => {
    setID(props.match.params.id);
    props.getDetail(id);
    // eslint-disable-next-line
  }, [props.match.params.id]);

  return (
    <Page>
      <div className="container-content">
        <Breadcrumb>
          <Breadcrumb.Section>User</Breadcrumb.Section>
          <Breadcrumb.Divider icon='right angle' />
          <Breadcrumb.Section link><Link to="/user">User List</Link></Breadcrumb.Section>
          <Breadcrumb.Divider icon='right angle' />
          <Breadcrumb.Section active>Detail User</Breadcrumb.Section>
        </Breadcrumb>

        <h2 style={{marginTop: '24px'}}>Detail User</h2>

        <Segment style={{padding: '15px 25px'}}>
          <h3>{props.user.name}</h3>

          <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
        </Segment>
      </div>
    </Page>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.users.detail,
  }
}

const mapDispatchToProps = (dispatch) => {
	return {
    getDetail: (id) => dispatch(getDetail(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserDetail);