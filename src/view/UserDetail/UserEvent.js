import React from 'react';
import { Grid, Table, Button } from 'semantic-ui-react';
import moment from 'moment';

const UserEvent = () => {
  let events = [
    { type: 'Login Success', device: 'Samsung S10', app: 'Neu Version 1.0.1', time: '2020-07-15T07:30:00.000Z'},
    { type: 'OTP Verified', device: 'Samsung S10', app: 'Neu Version 1.0.1', time: '2020-07-15T07:30:00.000Z'},
    { type: 'OTP Requested', device: 'Samsung S10', app: 'Neu Version 1.0.1', time: '2020-07-15T07:30:00.000Z'},
    { type: 'Login Success', device: 'Samsung S10', app: 'Neu Version 1.0.1', time: '2020-07-15T07:30:00.000Z'},
  ]
  return (
    <div>
      <Grid>
        <Grid.Row columns={2}>
          <Grid.Column>
            <h5 style={{margin: '5px 0 24px', fontSize: '12px'}}>Phone Number : 081333615391</h5>
          </Grid.Column>
          <Grid.Column style={{textAlign: 'right', color: '#46A997'}}>
            <span>1 - 11</span>
            <span style={{marginRight: '10px'}}> dari 40 </span>
            <Button icon='chevron left' circular size="mini" color="teal" style={{marginRight: '10px', backgroundColor: '#46A997', padding: '5px'}} />
            <Button icon='chevron right' circular size="mini" color="teal" style={{backgroundColor: '#46A997', padding: '5px'}} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Table basic="very">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Event</Table.HeaderCell>
            <Table.HeaderCell>Device</Table.HeaderCell>
            <Table.HeaderCell>Detail App</Table.HeaderCell>
            <Table.HeaderCell>Timestamp</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {events.length > 0 ?
            events.map((event, i) => (
              <Table.Row key={i}>
                <Table.Cell>{event.type}</Table.Cell>
                <Table.Cell>{event.device}</Table.Cell>
                <Table.Cell>{event.app}</Table.Cell>
                <Table.Cell>{moment(event.time).format('DD MMM YYY, HH:mm')}</Table.Cell>
              </Table.Row>
            ))
          :
            <Table.Row style={{height: '350px'}}>
              <Table.Cell textAlign="center" colspan={4}>
                No User Event Found
              </Table.Cell>
            </Table.Row>
          }
        </Table.Body>
      </Table>
    </div>
  );
};

export default UserEvent;