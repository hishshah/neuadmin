import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Page from '../Page';
import { Breadcrumb, Grid, Input, Segment, Table, Icon, Pagination, Button, Checkbox, Label, Modal, Form } from 'semantic-ui-react';
import './Admin.scss';
import moment from 'moment';
import _ from 'lodash';
import Swal from 'sweetalert2';
import { getList, create, deleteAdmin, editAdmin } from '../../actions/admin';
import Loading from '../Loading';

const Admin = (props) => {
  const { admins, meta } = props;
  
  const [search, setSearch] = useState('');
  const [selectAll, setSelectAll] = useState(false);
  const [selectedData, setSelectedData] = useState([]);
  const [showEditAdminModal, setShowEditAdminModal] = useState(false);
  const [editData, setEditData]= useState({name: '', email: '', role: ''});
  const [isEdit, setIsEdit]= useState(false);
  const [nameError, setNameError] = useState(false);
  const [emailError, setEmailError] = useState(false);
  const [data, setData] = useState(admins);
  const [isLoading, setIsLoading] = useState(false);

  const role = [
    { value: 'cs', key: 'CS', text: 'Customer Service (CS)' },
    { value: 'ops', key: 'Product Ops', text: 'Product Ops' },
    { value: 'finance', key: 'Finance', text: 'Finance' },
    { value: 'aml', key: 'AML/CFT', text: 'AML/CFT' },
  ];

  useEffect(() => {
    props.getList();
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    setData(admins);
  }, [admins]);

  const toggleSelectAll = (value) => {
    setSelectAll(value);
    const items = [...data];
    let checked = [];
    _.each(items, item => {
      item.checked = value;
      if (value) checked.push(item);
    });
    
    setData(items);
    setSelectedData(checked)
  }

  const toggleChecked = item => {
    const items = [...data];
    const key = items.map(x => x.id).indexOf(item.id);
    const selected = items[key];
    if (selected) {
      items[key].checked = !item.checked;
    }
    setData(items);

    let checked = [];
    _.each(items, item => {
      if (item.checked) checked.push(item)
    });
    setSelectedData(checked);

    if (checked.length === 0) {
      setSelectAll(false);
    } else if (checked.length === data.length) {
      setSelectAll(true);
    };
  };

  const openEditAdminModal = (type, item = {name: '', email: '', role: ''}) => {
    setIsEdit(type === 'edit');
    setEditData(item);
    setShowEditAdminModal(true);
    setNameError(false);
    setEmailError(false);
  }

  const handleChange = (type, value) => {
    let items = {...editData};
    items[type] = value;
    setEditData(items);

    switch (type) {
      case 'name':
        setNameError(value.length <= 3)
        break;
      case 'email':
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        setEmailError(!re.test(value));
        break;
      default:
        break;
    }
  }

  const submitForm = () => {
    const { id, name, email, active } = editData;
    let body = {
      name, email, active
    }
    if (!isEdit) {
      props.create(body).then(result => {
        if (_.get(result, 'payload.data.status', '') === 'success') {
          setShowEditAdminModal(false);
          setIsEdit(false);
          setEditData({name: '', email: '', role: ''});
          Swal.fire('', 'User successfully added').then(response => {
            props.getList(meta.page);
          })
        } else {
          Swal.fire('Error', '', 'error');
        };
      });
    } else {
      props.editAdmin(id, body).then(result => {
        if (_.get(result, 'payload.data.status', '') === 'success') {
          setShowEditAdminModal(false);
          setIsEdit(false);
          setEditData({name: '', email: '', role: ''});
          Swal.fire('', 'User successfully updated').then(response => {
            props.getList(meta.page);
          })
        } else {
          Swal.fire('Error', '', 'error');
        };
      });
    }
  }

  const deleteUser = () => {
    Swal.fire({
      title: `Delete ${selectedData.length} user(s)?`,
      text: 'If you delete a user, that user will no longer be able to access the Maqmur Administration web and will disappear from your admin list.',
      confirmButtonText: 'DELETE',
      cancelButtonText: 'CANCEL',
      showCancelButton: true,
      focusConfirm: false,
      focusCancel: false,
      customClass: {
        confirmButton: `confirm-deactivate-btn`,
        cancelButton: `cancel-deactivate-btn`,
      }
    }).then(result => {
      if (result.dismiss) return;
      
      setIsLoading(true);
      if (selectedData.length > 0) {
        let promises = [];
        _.each(selectedData, data => {
          promises.push(props.deleteAdmin(data.id));
        });
        Promise.all(promises).then(results => {
          let success = 0;
          _.each(results, res => {
            if (_.get(res, 'payload.data.status', '') === 'success') {
              success++;
            };
          });
          setIsLoading(false);
          Swal.fire('', `${success} user${success > 1 ? 's' : ''} ha${success > 1 ? 've' : 's'} been deleted`).then(response => {
            props.getList(meta.page);
          })
        })
      }
    })
  }

  const changePage = page => {
    props.getList(page);
  };

  const searchAdmin = (e) => {
    if (!e || !e.keyCode || e.keyCode === 13) {
      let query = search.length > 0 ? `email%3D${search}` : '';
      props.getList(meta.page, query);
    }
  };

  return (
    <Page>
      {isLoading &&
        <Loading />
      }
      <div className="container-content container-admin">
        <Breadcrumb>
          <Breadcrumb.Section>Admin Management</Breadcrumb.Section>
          <Breadcrumb.Divider icon='right angle' />
          <Breadcrumb.Section active>Admin User List</Breadcrumb.Section>
        </Breadcrumb>

        <Grid>
          <Grid.Row columns={2} style={{marginTop: '24px'}}>
            <Grid.Column>
              <h2>Admin User List</h2>
            </Grid.Column>
            <Grid.Column>
              <Input
                action={{
                  color: 'teal',
                  icon: 'search',
                  onClick: searchAdmin
                }}
                placeholder="Search email..."
                onSubmit={searchAdmin}
                fluid
                onChange={(e, {value}) => setSearch(value)}
                onKeyUp={(e) => searchAdmin(e)}
              />
            </Grid.Column>
          </Grid.Row>
        
          <Grid.Row as={Segment}>
            <Grid.Column style={{display: 'flex', alignItems: 'center'}}>
              <Button color="blue" onClick={() => openEditAdminModal('add')}>
                <Icon name="plus square outline" size="large" style={{opacity: '1', verticalAlign: 'sub'}} /> ADD NEW USER
              </Button>
              <Button color="red" style={{marginLeft: '40px'}} disabled={selectedData.length === 0} onClick={deleteUser}>
                <Icon name="trash alternate outline" size="large" style={{opacity: '1', verticalAlign: 'sub'}} /> DELETE
              </Button>
            </Grid.Column>
          </Grid.Row>

          <Grid.Row as={Segment} style={{marginBottom: '30px', padding: '15px'}}>
            <Grid.Column>
              <Grid.Row style={{overflowX: 'scroll'}}>
                <Grid.Column>
                  <Table basic="very" unstackable fixed>
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell style={{width: '30px'}}>
                          <Checkbox  
                            checked={selectAll} 
                            onChange={(e, {checked}) => toggleSelectAll(checked)}
                          />
                        </Table.HeaderCell>
                        <Table.HeaderCell>Name</Table.HeaderCell>
                        <Table.HeaderCell>Email</Table.HeaderCell>
                        <Table.HeaderCell>Created</Table.HeaderCell>
                        <Table.HeaderCell>Updated</Table.HeaderCell>
                        <Table.HeaderCell>Type Role</Table.HeaderCell>
                        <Table.HeaderCell>Action</Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                    <Table.Body>
                      {data.length > 0 ? 
                        data.map((item, i) => {
                          const role_label = _.find(role, r => r.value === item.role, false);
                          let role_color = 'grey';
                          switch (item.role) {
                            case 'cs':
                              role_color = 'teal';
                              break;
                            case 'ops':
                              role_color = 'orange';
                              break;
                            case 'finance':
                              role_color = 'green';
                              break;
                            case 'aml':
                              role_color = 'red';
                              break;
                            default:
                              break;
                          }
                          return (
                            <Table.Row key={i} verticalAlign='top'>
                              <Table.Cell style={{width: '30px'}}>
                                <Checkbox  
                                  checked={item.checked} 
                                  onChange={(e, {checked}) => toggleChecked(item, checked)}
                                />
                              </Table.Cell>
                              <Table.Cell>{item.name}</Table.Cell>
                              <Table.Cell>{item.email}</Table.Cell>
                              <Table.Cell>{moment(item.createdAt).format('DD MMMM YYYY')}</Table.Cell>
                              <Table.Cell>{moment(item.updatedAt).format('DD MMMM YYYY')}</Table.Cell>
                              <Table.Cell>
                                {role_label ? 
                                  <Label 
                                    circular 
                                    color={role_color} 
                                    style={{width: '90px'}}
                                  >
                                    {role_label.key}
                                  </Label>
                                :
                                  '-'
                                }
                              </Table.Cell>
                              <Table.Cell>
                                <Button basic fluid onClick={() => openEditAdminModal('edit', item)}>EDIT</Button>
                              </Table.Cell>
                            </Table.Row>
                          )
                        })
                      :
                        <Table.Row style={{height: '400px'}}>
                          <Table.Cell textAlign="center" colSpan={7}>
                            Admin not found
                          </Table.Cell>
                        </Table.Row>
                      }
                    </Table.Body>
                  </Table>
                </Grid.Column>
              </Grid.Row>
            </Grid.Column>
          </Grid.Row>

          {data.length > 0 &&
            <Grid.Row textAlign="center">
              <Grid.Column>
                <Pagination
                  activePage={_.get(meta, 'page', 1)}
                  onPageChange={(e, { activePage }) => changePage(activePage)}
                  boundaryRange={0}
                  totalPages={meta.totalPage}
                  firstItem={{ content: <Icon name='angle double left' />, icon: true }}
                  lastItem={{ content: <Icon name='angle double right' />, icon: true }}
                  prevItem={{ content: <Icon name='angle left' />, icon: true }}
                  nextItem={{ content: <Icon name='angle right' />, icon: true }}
                />
                <span style={{margin: '0 24px'}}>
                  {((meta.page-1) * 10) + 1}
                  - 
                  {meta.page === meta.totalPage ? meta.count : ((meta.page-1) * 10) + meta.limit}</span>
                <span style={{fontWeight: 'bold'}}>From {meta.count} Entries</span>
              </Grid.Column>
            </Grid.Row>
          }
        </Grid>
      </div>

      <Modal 
        open={showEditAdminModal} 
        onClose={() => setShowEditAdminModal(false)} 
        size="tiny"
        className="container-admin"
      >
        <Modal.Header>
          {isEdit ? 'EDIT' : 'ADD NEW'} USER
          <span style={{float: 'right', cursor: 'pointer', fontSize: '25px'}} onClick={() => setShowEditAdminModal(false)}>&times;</span>
        </Modal.Header>
        <Modal.Content>
          <Form className="admin-form">
            <Form.Field style={{marginBottom: '24px'}}>
              <div style={{marginBottom: '4px'}}>Name</div>
              <Input 
                placeholder="Full Name" 
                value={editData.name}
                onChange={(e, {value}) => handleChange('name', value)} 
              />
              {nameError &&
                <div style={{marginTop: '4px', color: '#EC1E24', fontSize: '10px'}}>At least 3 characters</div>
              }
            </Form.Field>
            <Form.Field style={{marginBottom: '24px'}}>
              <div style={{marginBottom: '4px'}}>Email</div>
              <Input 
                placeholder="Email Address" 
                type="email" 
                value={editData.email}
                onChange={(e, {value}) => handleChange('email', value)} 
              />
              {emailError &&
                <div style={{marginTop: '4px', color: '#EC1E24', fontSize: '10px'}}>The correct e-mail format: name@email.com</div>
              }
            </Form.Field>
            <Form.Field style={{marginBottom: '24px'}}>
              <div style={{marginBottom: '6px'}}>Status</div>
              <Checkbox 
                toggle 
                checked={editData.active} 
                label={editData.active ? 'Active' : 'Not Active'}
                onChange={(e) => handleChange('active', !editData.active)}
              />
            </Form.Field>
            {/* <Form.Field style={{marginBottom: '24px'}}>
              <div style={{marginBottom: '4px'}}>Role</div>
              <Form.Select 
                placeholder="Select Role" 
                options={role} 
                value={editData.role}
                onChange={(e, {value}) => handleChange('role', value)} 
              />
            </Form.Field> */}
          </Form>
        </Modal.Content>
      
        <Modal.Actions>
          <Button 
            onClick={submitForm}
            color="teal"
            disabled={!editData.name || !editData.email}
          >
            {isEdit ? 'SAVE CHANGE' : 'ADD USER'}
          </Button>
        </Modal.Actions>
      </Modal>
    </Page>
  );
};

const mapStateToProps = state => {
  return {
    admins: state.admin.list,
    meta: state.admin.meta
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getList: (page, search) => dispatch(getList(page, search)),
    create: (data) => dispatch(create(data)),
    deleteAdmin: (id) => dispatch(deleteAdmin(id)),
    editAdmin: (id, data) => dispatch(editAdmin(id, data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Admin);