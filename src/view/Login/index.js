import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import './Login.scss'
import logo from '../../assets/logo/maqmur_logo.svg';
import { GoogleLogin } from 'react-google-login';
import { login } from '../../actions/auth';
import Swal from 'sweetalert2';
import _ from 'lodash';
import { GOOGLE_CLIENT_ID } from '../../global';

const Login = (props) => {
  useEffect(() => {
    if (props.auth.isAuthenticate) {
      props.history.push('/user');
    };
    // eslint-disable-next-line
  }, []);

  const responseGoogle = (response) => {
    if (response.tokenId) {
      props.login(response.tokenId).then(result => {
        if (result.type === 'LOGIN_FAIL') {
          Swal.fire('Error!', _.get(result, 'error.response.data.message'), 'error');
        } else {
          props.history.push('/user');
        }
      });
    }
  }

  return (
    <div className="login">
      <div className="login-container">
        <img src={logo} alt="logo" className="login-logo" />

        <h3>Sign In</h3>
        <p>With your Google Account</p>
        {/* <Button fluid style={{backgroundColor: '#fff', border: '1px solid #EBEDF8', boxShadow: '0 2px 4px rgba(0,0,0,0.15)'}}>
          <img src={google} alt="google" style={{marginRight: '30px', verticalAlign: 'middle'}} />
          <span>Sign in with Google</span>
        </Button> */}
        <GoogleLogin
          clientId={GOOGLE_CLIENT_ID}
          buttonText="Sign in with Google"
          className="ui fluid button login-btn"
          onSuccess={responseGoogle}
          onFailure={responseGoogle}
          cookiePolicy={'single_host_origin'}
        />
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth.auth
  }
}

const mapDispatchToProps = (dispatch) => {
	return {
    login: (tokenId) => dispatch(login(tokenId)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);