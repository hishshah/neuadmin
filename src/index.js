import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { createStore, applyMiddleware, compose } from 'redux' 
import { Provider } from 'react-redux'
import axios from 'axios'
import thunk from 'redux-thunk'
import axiosMiddleware from 'redux-axios-middleware'
import { loadState, saveState } from './local-storage'

import rootReducer from './reducers/rootReducer'
import './index.css'
import { unregister } from './serviceWorker'

const persistedState = loadState()

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const axiosCreator = {
  baseURL: 'https://api.maqmur.com/',
  responseType: 'json',
};

const middlewareConfig = {
  interceptors: {
    request: [
      ({getState, dispatch, getSourceAction}, req) => {
        if (getState().auth.token) {
          req.headers['Authorization'] = getState().auth.token
        }
        return req;
      }
    ]    
  }
};

const client = axios.create(axiosCreator);

const store = createStore(
  rootReducer, 
  persistedState,
  composeEnhancers(
    applyMiddleware(thunk, axiosMiddleware(client, middlewareConfig))
  )
);

store.subscribe(() => {
	saveState(store.getState())
})

window.snapSaveState = () => ({
  __PRELOADED_STATE__: store.getState()
})

const render = Component => {
  return ReactDOM.render(
    <Provider store={store}><App /></Provider>,
    document.getElementById('root')
  )
}

render(App)

if (module.hot) {
  module.hot.accept('./App', () => {
    const NextApp = require('./App').default
    render(NextApp)
  })
}

unregister()