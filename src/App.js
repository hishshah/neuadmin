import React, {Component} from 'react';
// import { connect } from 'react-redux';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import User from './view/User';
import Login from './view/Login';
import Order from './view/Order';
import UserDetail from './view/UserDetail';
import Payment from './view/Payment';
import OrderDetail from './view/OrderDetail';
import Recharges from './view/Recharges';
import Finance from './view/Finance';
import Admin from './view/Admin';

class App extends Component {
  render() {
    // const { isAuthenticate } = this.props
  
    return (
      <Router>
        <div>
          <Switch>
            <Route exact path='/' component={User} />
            <Route path='/user/:id' component={UserDetail} />
            <Route path='/user' component={User} />
            <Route path='/payment' component={Payment} />
            <Route path='/order/:id' component={OrderDetail} />
            <Route path='/order' component={Order} />
            <Route path='/recharges' component={Recharges} />
            <Route path='/finance' component={Finance} />
            <Route path='/admin' component={Admin} />
            <Route path='/login' component={Login} />
            <Route component={User} />
          </Switch>
        </div>
      </Router>
    )
  }
};

// const mapStateToProps = state => {
//   return {
//     // isAuthenticate: state.login.isAuthenticate
//   }
// };

// export default connect(mapStateToProps)(App)
export default App;