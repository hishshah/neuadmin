const url = '/product';
export const GET = 'RECHARGES_GET';
export const UPDATE = 'RECHARGES_UPDATE';
export const SYNC = 'RECHARGES_SYNC';

export const getList = (page = 1, search = '', query = '') => {
  return {
    type: GET,
    payload: {
      request: {
        url: `${url}?limit=10&page=${page}${search.length > 0 ? '&search=' + search : ''}${query.length > 0 ? '&q=' + query : ''}`,
        method: 'get'
      }
    }
  }
}

export const update = (id, data) => {
  return {
    type: UPDATE,
    id,
    data,
    payload: {
      request: {
        url: `${url}/${id}`,
        method: 'put',
        data
      }
    }
  }
}

export const sync = () => {
  return {
    type: SYNC,
    payload: {
      request: {
        url: `${url}/sync-data`,
        method: 'get'
      }
    }
  }
}