const url = '/user';
export const GET = 'USER_GET';
export const UPDATE = 'USER_UPDATE';
export const GET_DETAIL = 'USER_GET_DETAIL';
export const RESET_DETAIL = 'USER_RESET_DETAIL';

export const getList = (page = 1, search = '') => {
  return {
    type: GET,
    payload: {
      request: {
        url: `${url}?limit=10&page=${page}${search.length > 0 ? '&search=' + search : ''}`,
        method: 'get'
      }
    }
  }
}

export const update = (item) => {
  return {
    type: UPDATE,
    item,
    payload: {
      request: {
        url: `${url}/${item.id}`,
        method: 'put',
        data: { 
          active: item.active
        }
      }
    }
  }
}

export const getDetail = (id) => {
  return {
    type: GET_DETAIL,
    payload: {
      request: {
        url: `${url}/${id}?populate=userXfers`,
        method: 'get'
      }
    }
  }
}