const url = '/file';
export const UPLOAD = 'UPLOAD_FILE';

export const uploadFile = (data) => {
  return {
    type: UPLOAD,
    payload: {
      request: {
        url: `${url}`,
        method: 'post',
        data
      }
    }
  }
}