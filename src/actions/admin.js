const url = '/admin';
export const GET = 'ADMIN_GET';
export const UPDATE = 'ADMIN_UPDATE';
export const CREATE = 'ADMIN_CREATE';
export const DELETE = 'ADMIN_DELETE';
export const EDIT = 'ADMIN_EDIT';

export const getList = (page = 1, search = '') => {
  return {
    type: GET,
    payload: {
      request: {
        url: `${url}?limit=10&page=${page}${search.length > 0 ? '&search=' + search : ''}`,
        method: 'get'
      }
    }
  }
}

export const update = (index, item) => {
  return {
    type: UPDATE,
    index,
    item,
  }
}

export const create = (data) => {
  return {
    type: CREATE, 
    payload: {
      request: {
        url,
        method: 'post',
        data
      }
    }
  }
}

export const deleteAdmin = (id) => {
  return {
    type: DELETE, 
    payload: {
      request: {
        url: `${url}/${id}`,
        method: 'delete'
      }
    }
  }
}

export const editAdmin = (id, data) => {
  return {
    type: EDIT, 
    payload: {
      request: {
        url: `${url}/${id}`,
        method: 'put',
        data
      }
    }
  }
}