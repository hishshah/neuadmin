export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';

export const login = (tokenId) => {
  return {
    type: LOGIN,
    payload: {
      request: {
        url: `/admin/auth/oauth`,
        method: 'post',
        data: { tokenId }
      }
    }
  }
}

export const logout = () => {
  return {
    type: LOGOUT
  }
}