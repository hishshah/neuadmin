const url = '/transaction';
export const GET = 'ORDER_GET';
export const GET_DETAIL = 'ORDER_GET_DETAIL';
export const RESET_DETAIL = 'ORDER_RESET_DETAIL';

export const getList = (page = 1, search = '', query = '') => {
  return {
    type: GET,
    payload: {
      request: {
        url: `${url}?sort=-createdAt&limit=10&populate=user%20product%20payment&page=${page}&q=type%3Dproduct${query.length > 0 ? '%26' + query : ''}${search.length > 0 ? '&search=' + search : ''}`,
        method: 'get'
      }
    }
  }
}

export const getDetail = (id) => {
  return {
    type: GET_DETAIL,
    payload: {
      request: {
        url: `${url}/${id}?populate=user%20product%20payment`,
        method: 'get'
      }
    }
  }
}

export const downloadOrder = (page, query) => {
  return {
    type: 'DOWNLOAD_ORDER',
    payload: {
      request: {
        url: `${url}?sort=-createdAt&limit=1000&populate=user%20product%20payment&page=${page}&q=type%3Dproduct%26${query}`,
        method: 'get'
      }
    }
  }
}