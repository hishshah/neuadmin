export const TOGGLE = 'SIDEBAR_TOGGLE';

export const toggleSidebar = () => {
  return {
    type: TOGGLE,
  }
}