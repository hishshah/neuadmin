const url = '/transaction';
export const GET = 'PAYMENT_GET';
export const GET_DETAIL = 'PAYMENT_GET_DETAIL';
export const RESET_DETAIL = 'PAYMENT_RESET_DETAIL';
export const POST_REFUND = 'PAYMENT_POST_REFUND';
export const UPDATE_STATUS = 'PAYMENT_UPDATE_STATUS';
export const CHECK_NAME = 'PAYMENT_CHECK_NAME';

export const getList = (page = 1, search = '', query = '') => {
  return {
    type: GET,
    payload: {
      request: {
        url: `${url}?sort=-createdAt&limit=10&populate=user%20product%20payment&page=${page}&q=type%3Dtransfer${query.length > 0 ? '%26' + query : ''}${search.length > 0 ? '&search=' + search : ''}`,
        method: 'get'
      }
    }
  }
}

export const getDetail = (id) => {
  return {
    type: GET_DETAIL,
    payload: {
      request: {
        url: `${url}/${id}?populate=user`,
        method: 'get'
      }
    }
  }
}

export const refundPayment = (data) => {
  return {
    type: POST_REFUND,
    payload: {
      request: {
        url: `${url}/refund`,
        method: 'post',
        data
      }
    }
  }
}

export const updateStatus = (id, data) => {
  return {
    type: UPDATE_STATUS,
    payload: {
      request: {
        url: `${url}/refund/action/${id}`,
        method: 'put',
        data
      }
    }
  }
}

export const checkName = (data) => {
  return {
    type: CHECK_NAME,
    payload: {
      request: {
        url: `${url}/transfer/namecheck`,
        method: 'post',
        data
      }
    }
  }
}

export const downloadPayment = (page, query) => {
  return {
    type: 'DOWNLOAD_PAYMENT',
    payload: {
      request: {
        url: `${url}?sort=-createdAt&limit=1000&populate=user%20product%20payment&page=${page}&q=type%3Dtransfer%26${query}`,
        method: 'get'
      }
    }
  }
}