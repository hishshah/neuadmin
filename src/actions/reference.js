const url = '/reference';
export const GET_BANKS = 'REFERENCE_GET_BANKS';

export const getBanks = (search = '') => {
  return {
    type: GET_BANKS,
    payload: {
      request: {
        url: `${url}/bank?${search.length > 0 ? 'search=' + search : ''}`,
        method: 'get'
      }
    }
  }
}